@echo off
::O símbolo @ faz com que o comando não seja mostrado
::O comando ECHO off desabilita permanentemente a visualização dos comandos executados (ou até que seja usado echo on)

color 0B
::Muda a cor da janela/fonts. o 1º dígito representa a cor do fundo, o 2º representa a cor da font.

title Compilar C++ com MinGW
::Dá um título à janela
setlocal enableextensions
setlocal enabledelayedexpansion

set src_folder=src
set res_folder=res
set include_folder=include
set lib_folder=lib

:start
echo Olá, %USERNAME%
echo O que pretendes fazer?
echo.
echo 1. Compilar e Montar o projecto.
echo 2. Correr o projecto.
echo 3. Compilar todos os ficheiros.
echo 4. Compilar apenas ficheiros especificados.
echo 5. Montar o projecto (linking).
echo 6. Limpar ficheiros *.o e *.exe
echo 7. Limpar ficheiros *.o
echo. 
echo 0. Sair
echo.


set /p choice="Submeter escolha: "
if "%choice%"=="1" goto all
if "%choice%"=="2" goto run
if "%choice%"=="3" goto compile
if "%choice%"=="4" goto compileS
if "%choice%"=="5" goto link
if "%choice%"=="6" goto clean
if "%choice%"=="7" goto clean_o
if "%choice%"=="0" exit
echo Escolha inválida: %choice%
echo.
pause
cls
goto start

:all
::Compila e Monta
set /p name="Qual o nome a dar ao ficheiro executável? "
windres -r %res_folder%\icon.rc icon.o
::g++ -o %name% -m32 -Wall *.cpp icon.o -D FREEGLUT_STATIC -I"C:\MinGW\freeglut\include" -L"C:\MinGW\freeglut\lib" -lfreeglut_static -mwindows -lwinmm -lopengl32 -lglu32 -lgdi32 -static-libgcc -static-libstdc++
g++ -o %name% -m32 -Wall %src_folder%\*.cpp icon.o -isystem%include_folder% -L%lib_folder% %lib_folder%\glut32.lib -mwindows -lwinmm -lopengl32 -lglu32 -lgdi32 -static-libgcc -static-libstdc++
:: -Wall para mostrar todos os warnings. -mwindows para não mostrar a consola.
::Faz pausa para o utilizador ver o resultado da compilação e montagem (se não aparecer nada é porque correu tudo bem)
::-static-libgcc -static-libstdc++ para não serem necessários dll's extra na execução do programa.
pause
::Limpa o ecrã
cls
::Volta ao início
goto start

:run
if "%name%"=="" set /p name="Qual o nome do ficheiro executável? "
%name%
pause
::Limpa o ecrã
cls
::Volta ao início
goto start

:compile
::Compila todos
windres -r %res_folder%\icon.rc icon.o
g++ -Wall -pedantic -c %src_folder%\*.cpp -isystem%include_folder%
::-mwindows, -lopengl32, -lglu32 não são necessários na fase de compilação.
::Faz pausa para o utilizador ver o resultado da compilação (se não aparecer nada é porque correu tudo bem)
pause
::Limpa o ecrã
cls
::Volta ao início
goto start

:compileS
::Compila apenas alguns ficheiros.
set /p files="Quais os ficheiros a compilar(separar com espaços)? "
g++ -Wall -pedantic -c %files% -isystem%include_folder%
::-mwindows, -lopengl32, -lglu32 não são necessários na fase de compilação.
::Faz pausa para o utilizador ver o resultado da compilação (se não aparecer nada é porque correu tudo bem)
pause
::Limpa o ecrã
cls
::Volta ao início
goto start

:link
::Monta apenas
set /p name="Qual o nome a dar ao ficheiro executável? "
g++ -o %name% *.o -m32 -mwindows -lwinmm  %lib_folder%\glut32.lib -lopengl32 -lglu32 -static-libgcc -static-libstdc++
::glut32.lib só é usado na fase do link dos objectos.
::-lglu32 não se revelou necessário.
:: -lwinmm para usar a função PlaySound
::Faz pausa para o utilizador ver o resultado da montagem (se não aparecer nada é porque correu tudo bem)
::-static-libgcc -static-libstdc++ para nÒo serem necessßrios dll's extra na execuþÒo do programa.
pause
::Limpa o ecrã
cls
::Volta ao início
goto start

:clean
for /r %%a in (*.o) do ( del "%%a" )
for /r %%a in (*.exe) do ( del "%%a" )
pause
cls
goto start

:clean_o
for /r %%a in (*.o) do ( del "%%a" )
pause
cls
goto start