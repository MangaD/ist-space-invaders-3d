# IST - Space Invaders 3D

This is a game project using C++ programming language and GLUT, the OpenGL Utility Toolkit. This game was done for the Graphical Computation class at Instituto Superior Técnico in the 2012-13 academic year. The game is a reproduction of the classic Space Invaders, with the peculiarity of being 3D.

### Screenhots
2D View:

[![screenshot.png](https://s6.postimg.cc/3xck7lq5d/screenshot.png)](https://postimg.cc/image/3kl61f7vh/)

3D View:

[![screenshot2.png](https://s6.postimg.cc/i528w92u9/screenshot2.png)](https://postimg.cc/image/m1fks8ntp/)

3D View with flashlight:

[![screenshot3.png](https://s6.postimg.cc/4cnu0mc2p/screenshot3.png)](https://postimg.cc/image/526mczcm5/)

Wireframes (hitboxes):

[![screenshot4.png](https://s6.postimg.cc/gft5o6n4x/screenshot4.png)](https://postimg.cc/image/l1p9wj8nx/)

Ship destroyed:

[![screenshot5.png](https://s6.postimg.cc/siyhbqy75/screenshot5.png)](https://postimg.cc/image/hjda057rx/)


### Compiling

This game was developed in Visual Studio and for Windows, although it can be easily ported for Mac OS and Linux with a few changes. It can also be compiled using MinGW and the batch script makes it easier, although a Makefile would be the right way to go. The game is also poorly programmed, not making use of many of the C++ features.


### Credits

#### Developers
* Daniel Lascas
* David Gonçalves
* Ricardo Moura

#### Thanks
* [DL Sounds](http://www.dl-sounds.com/royalty-free/star-commander1/) for the sound