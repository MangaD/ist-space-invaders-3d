/**
 * Declaração da classe Camera3D que herda da classe abstracta Camera. Mostra a cena em perspectiva.
 *
 * //Camera3D.h
 *
 * @author Grupo 18, Turno 3ª 17:30
 * @author Daniel Freire Lascas - 73824
 * @author David Manuel Sales Goncalves - 73891
 * @author Ricardo Manuel Mota de Moura - 74005
 */

#pragma once//Para evitar incluir esta classe mais que uma vez no processo de compilação, usamos #pragma once (podiamos usar #include guards).

#include "Camera.h"
#include "Jogo.h"
#include <windows.h>

#if defined(__APPLE__) || defined(MACOSX)
	#include <GLUT/glut.h>
#else
	#include <GL/glut.h>
#endif

class Camera3D: public Camera {
private:
	static Camera3D *camera;
	Camera3D();
	float naveX;
	float naveY;
	float naveZ;
public:
	static Camera3D *getCamera();
	void proj(int width, int height);
};
