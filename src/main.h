/**
 * Projecto CG - Space Invaders
 *
 * //main.h
 *
 * @author Grupo 18, Turno 3ª 17:30
 * @author Daniel Freire Lascas - 73824
 * @author David Manuel Sales Goncalves - 73891
 * @author Ricardo Manuel Mota de Moura - 74005
 */

#pragma once

#include <windows.h>
#include <mmsystem.h>
#include <time.h>//para o srand
#include <sstream>
#include <string>
#include <list>
#include "Explosion.h"

#if defined(__APPLE__) || defined(MACOSX)
	#include <GLUT/glut.h>
#else
	#include <GL/glut.h>
#endif

#include "Jogo.h"

#include "Camera2D.h"

#include "Camera3D.h"

#include "Camera3DFollow.h"

#include "BMPReader.h"

/* myReshape function declaration*/
void myReshape(GLsizei w, GLsizei h);

/* myDisplay function declaration*/
void myDisplay(void);

/* keyPressed function declaration */
void keyPressed(unsigned char key, int x, int y);

/* keyUp function declaration */
void keyUp(unsigned char key, int x, int y);

/* keySpecial function declaration */
void keySpecial(int key, int x, int y);

/* alienTimer function declaration */
void alienTimer(int value);

/* balaTimer function declaration */
void balaTimer(int value);

/* balaAliensTimer function declaration */
void balaAliensTimer(int value);

/* keySpecialUp function declaration */
void keySpecialUp (int key, int x, int y);

/* naveTimer function declaration */
void naveTimer(int value);

/* lightInit function declaration */
void lightInit();

/* LoadTexture function declaration */
GLuint LoadTexture(const char * filename);

/* background function declaration */
void background();

/* glutStrokeString function declaration */
void glutStrokeString (const void * font, char *string, double translateX, double translateY, double translateZ, 
					   double ScaleX, double ScaleY, double ScaleZ,
					   float Red, float Green, float Blue, 
					   unsigned int Bold, bool Italic);

/* glutBitmapString function declaration */
void glutBitmapString (void* font, const string &string, double translateX, double translateY);

/* draw2Dover3D function declaration */
void draw2Dover3D(void (_cdecl *func) (void));

/* draw2Dunder3D function declaration */
void draw2Dunder3D(void (_cdecl *func) (void));

/* drawPause function declaration */
void drawPause();

/* HUD - Heads-up Display function declaration */
void HUD();

/* HDD - Heads-down Display function declaration */
void HDD();

/* Reset Timers function declaration*/
void resetTimers();

/* explosionsTimer function declaration*/
void explosionsTimer(const int value);