/**
 * Declaração da classe abstracta Camera que será herdada pela Camera2D, Camera3D e Camera3DFollow.
 *
 * //Camera.h
 *
 * @author Grupo 18, Turno 3ª 17:30
 * @author Daniel Freire Lascas - 73824
 * @author David Manuel Sales Goncalves - 73891
 * @author Ricardo Manuel Mota de Moura - 74005
 */

#pragma once//Para evitar incluir esta classe mais que uma vez no processo de compilação, usamos #pragma once (podiamos usar #include guards).

class Camera{
public:
	virtual void proj(int width, int height) = 0;   /*Ao declarar uma função virtual pura na classe, a classe (e a função) passa a ser considerada abstracta. 
							   Não se pode instanciar um objecto Camera. A função proj() tem que ser redefinida nas subclasses, caso
							   contrário estas serão também abstractas */

};//Classes em C++, tal como as estruturas (struct), levam um ';'
