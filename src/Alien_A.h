/**
 * Declaração da classe Alien_A que herda da classe abstracta Alien.
 *
 * //Alien_A.h
 *
 * @author Grupo 18, Turno 3ª 17:30
 * @author Daniel Freire Lascas - 73824
 * @author David Manuel Sales Goncalves - 73891
 * @author Ricardo Manuel Mota de Moura - 74005
 */

#pragma once//Para evitar incluir esta classe mais que uma vez no processo de compilação, usamos #pragma once (podiamos usar #include guards).

#include <windows.h>
#include "Alien.h"

#if defined(__APPLE__) || defined(MACOSX)
	#include <GLUT/glut.h>
#else
	#include <GL/glut.h>
#endif

class Alien_A: public Alien {
public:
	Alien_A(float x, float y, float z);
	~Alien_A();
	void draw(bool wireframe);
	void drawB(bool wireframe);

};