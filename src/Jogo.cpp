/**
 * Implementação da classe Jogo (singleton) - Cria todos os objectos a serem desenhados.
 *
 * //jogo.cpp
 *
 * @author Grupo 18, Turno 3ª 17:30
 * @author Daniel Freire Lascas - 73824
 * @author David Manuel Sales Goncalves - 73891
 * @author Ricardo Manuel Mota de Moura - 74005
 */

#include "Jogo.h"




//Inicialização do único objecto do tipo Jogo com um ponteiro estático.
Jogo *Jogo::jogo = 0;


//Implementação do método estático getJogo() que devolve a única instância existente do tipo Jogo.
Jogo &Jogo::getJogo(){
	if(!jogo)
		jogo = new Jogo();
	return *jogo;
}

//Construtor da classe Jogo que inicializa todos os objectos.
Jogo::Jogo(){

	//Cria nave.
	n = new Nave(0.0f, -4.0f, 0.0f);

	//Cria 4 escudos
	for(float i=0.0f, f = -3.76f; i<(float)numEscudos; i++, f+=2.5f){// i corresponde ao número de escudos, f à posição de cada escudo.
		e[(int)i]=new Escudo(f, -2.0f, 0.0f);
	}
	posAliens=1.0f;
	//Cria 5*11 Aliens
	for(int i=0, j=0; !(i==(numLinhas-1) && j==numColunas); j++) {
		if(j==numColunas) { //Ao chegar à coluna 11, a coluna passa a ser 0, a linha passa à seguinte.
			j=0;
			i++;
		}
		//Como os aliens tem em comum a posicão Y (os que pertencem à mesma linha) então podemos fazer com que a posição X dependa da coluna dai aparecere
		//o "+ 0.8*j" pois é o que é diferente entre os aliens da mesma linha, e usando a mesma lógica para os que pertencem à mesma coluna temos "+ 0.8*i"
		if(i>=4)
			a[i][j] = new Alien_C((-4.0f+0.8*j)+0.01f, posAliens+0.8*i, 0.0f);
		else if (i<4 && i >=2)
			a[i][j] = new Alien_B((-4.0f+0.8*j)+0.01f, posAliens+0.8*i, 0.0f);
		else if (i<2)
			a[i][j] = new Alien_A(-4.0f+0.8*j, posAliens+0.8*i, 0.0f);
	}

	//A variável dirAliens é usada para saber qual o sentido no qual os aliens se têm que mover.
	dirAliens = 1;
	animateAliens=false;
	//Limite superior da bala = onde a bala desaparece. Limite inferior = distância permitida entre balas.
	Jogo::balaLimiteInf=2.5f;
	Jogo::balaLimiteSup=5.5f;
	//
	score = 0;
	vidas = 3;

}

void Jogo::nextLevel(){
	vidas+=1;
	//Cria 5*11 Aliens
	if(posAliens-0.5 > -1.0f)
		posAliens-=0.5;
	for(int i=0, j=0; !(i==(numLinhas-1) && j==numColunas); j++) {
		if(j==numColunas) { //Ao chegar à coluna 11, a coluna passa a ser 0, a linha passa à seguinte.
			j=0;
			i++;
		}
		//Como os aliens tem em comum a posicão Y (os que pertencem à mesma linha) então podemos fazer com que a posição X dependa da coluna dai aparecere
		//o "+ 0.8*j" pois é o que é diferente entre os aliens da mesma linha, e usando a mesma lógica para os que pertencem à mesma coluna temos "+ 0.8*i"
		if(i>=4)
			a[i][j] = new Alien_C((-4.0f+0.8*j)+0.01f, posAliens+0.8*i, 0.0f);
		else if (i<4 && i >=2)
			a[i][j] = new Alien_B((-4.0f+0.8*j)+0.01f, posAliens+0.8*i, 0.0f);
		else if (i<2)
			a[i][j] = new Alien_A(-4.0f+0.8*j, posAliens+0.8*i, 0.0f);
	}
}

//Cria novo jogo
void Jogo::novoJogo(){

	//Elimina as balas da nave
	jogo->balasNave.clear();//A lista fica com tamanho 0 e os seus elementos são destruidos.

	//Elimina as balas dos aliens
	jogo->balasAliens.clear();

	//Elimina as explosões
	jogo->listaExplosoes.clear();

	//Elimina a nave
	if(jogo->n!=0)
		delete jogo->n;
	//Elimina Escudos
	for(int i=0; i<numEscudos; i++){
		if(jogo->e[i]!=0)
			delete jogo->e[i];
	}
	//Elimina aliens
	for(int i=0, j=0; !(i==(numLinhas-1) && j==numColunas); j++){
		if(j==numColunas){
			i++;
			j=0;
		}
		if(jogo->a[i][j]!=0)
			delete jogo->a[i][j];
	}
	delete jogo;
	jogo = new Jogo();
}

unsigned int Jogo::getScore(){
	return score;
}

void Jogo::increaseScore(unsigned int value){
	score+=value;
}

int Jogo::getVidas(){
	return vidas;
}

void Jogo::decreaseVidas(unsigned int value){
	vidas-=value;
}

Nave* Jogo::getNave(){
	return n;
}

void Jogo::setNave(Nave* nave){
	if(!n)
		delete n;
	n=nave;
}

std::list<ParticleSystem*> &Jogo::getListaExplosoes(){
	return listaExplosoes;
}

bool Jogo::alienDispara(int value){
	
	float balaAlienLimiteInf = -8.0f;

	if(value == 1){ //Se o valor for 1, uma nova bala é criada num alien aleatório.

		Alien *alien_inf = 0;

		int random = rand() % numColunas + 0;//De 0 a numColunas-1
		
		for(int i=0, j=random, k=0; k<numColunas; i++) {
			if(i==numLinhas) { //A variável k serve para saber se o ciclo já verificou todas as colunas.
				i=0;
				j++;
				k++;
			}
			if(j==numColunas){
				j=0;
				k++;
			}
			if(a[i][j]!=0){
				alien_inf=a[i][j];
				break;
			}
		}
		if(alien_inf != 0){
			Bala *bala = new Bala(alien_inf->getX(), alien_inf->getY(), alien_inf->getZ());
			balasAliens.push_back(bala);
		}
	}
	else{
		//movem-se todas as balas
		std::list<Bala*>::iterator i;
		for(i=balasAliens.begin(); i != balasAliens.end(); ++i){
			(*i)->moveY(-0.3);
		}
		//elimina-se a bala que tenha ultrapassado o limite inferior
		for(i=balasAliens.begin(); i != balasAliens.end();){
			if((*i)->getY() < balaAlienLimiteInf){
				i = balasAliens.erase(i);
			}
			else
				++i;
		}
		//Verifica colisões
		colisaoBalasComEscudo(balasAliens);

		bool resultado=colisaoBalasComNave();
		if(resultado)
			return true;
	}
	return false;
}

bool Jogo::colisaoBalasComNave(){
	//Verifica se alguma bala atingiu a nave
	//coordenadas da nave
	float nX;
	float nY;
	float nZ;
	float nR;
	//coordenadas da bala
	float bX;
	float bY;
	float bZ;
	float bR;
	float d2;
	std::list<Bala*>::iterator s;
	for(s=balasAliens.begin(); s != balasAliens.end();){
		if(n!=0){
			nX = n->getX();
			nY = n->getY();
			nZ = n->getZ();
			nR = n->getRadius();
			bX = (*s)->getX();
			bY = (*s)->getY();
			bZ = (*s)->getZ();
			bR = (*s)->getRadius();
			d2 = ((nX - bX) * (nX - bX)) + ((nY - bY) * (nY - bY)) + ((nZ - bZ) * (nZ - bZ));
			if (d2 <= bR*bR + nR*nR){
				s = balasAliens.erase(s);
				return true;
			}
			else
				++s;
		}
	}
	return false;
}

void Jogo::colisaoBalasComEscudo(std::list<Bala*> &lista){
	//Verifica se alguma atingiu algum escudo
	//coordenadas do escudo
	float eX, eY, eZ;
	float e_big_left, e_big_right, e_big_bot, e_big_top, e_big_near, e_big_far;
	float e_smallL_left, e_smallL_right, e_smallL_bot, e_smallL_top, e_smallL_near, e_smallL_far;
	float e_smallR_left, e_smallR_right, e_smallR_bot, e_smallR_top, e_smallR_near, e_smallR_far;
	//coordenadas da bala
	float bX;
	float bY;
	float bZ;
	float bR;
	int k=0;
	std::list<Bala*>::iterator i;
	for(i=lista.begin(); i != lista.end();){
		for(k=0; k<numEscudos; k++){
			if(e[k]!=0){
				eX = e[k]->getX();
				eY = e[k]->getY();
				eZ = e[k]->getZ();
				//
				e_big_left=e[k]->x_big_left + eX;
				e_big_right=e[k]->x_big_right + eX;
				e_big_bot=e[k]->y_big_bot + eY;
				e_big_top=e[k]->y_big_top + eY;
				e_big_near=e[k]->z_big_near + eZ;
				e_big_far=e[k]->z_big_far + eZ;
				//
				e_smallL_left=e[k]->x_smallL_left + eX;
				e_smallL_right=e[k]->x_smallL_right + eX;
				e_smallL_bot=e[k]->y_smallL_bot + eY;
				e_smallL_top=e[k]->y_smallL_top + eY;
				e_smallL_near=e[k]->z_smallL_near + eZ;
				e_smallL_far=e[k]->z_smallL_far + eZ;
				//
				e_smallR_left=e[k]->x_smallR_left + eX;
				e_smallR_right=e[k]->x_smallR_right + eX;
				e_smallR_bot=e[k]->y_smallR_bot + eY;
				e_smallR_top=e[k]->y_smallR_top + eY;
				e_smallR_near=e[k]->z_smallR_near + eZ;
				e_smallR_far=e[k]->z_smallR_far + eZ;
				//
				bX = (*i)->getX();
				bY = (*i)->getY();
				bZ = (*i)->getZ();
				bR = (*i)->getRadius();
				if(
					(((bX+bR)>e_big_left && (bX-bR)<e_big_right) && ((bY+bR)>e_big_bot && (bY-bR)<e_big_top) && ((bZ+bR)>e_big_far && (bZ-bR)<e_big_near))

					|| (((bX+bR)>e_smallL_left && (bX-bR)<e_smallL_right) && ((bY+bR*2)>e_smallL_bot && (bY-bR*2)<e_smallL_top) && ((bZ+bR)>e_smallL_far && (bZ-bR)<e_smallL_near))

					|| (((bX+bR)>e_smallR_left && (bX-bR)<e_smallR_right) && ((bY+bR*2)>e_smallR_bot && (bY-bR*2)<e_smallR_top) && ((bZ+bR)>e_smallR_far && (bZ-bR)<e_smallR_near))
					){
					i = lista.erase(i);
					if(e[k]->resistance - 1 > 0)
						e[k]->resistance -= 1;
					else{
						delete e[k];
						e[k]=0;
					}
					break;
				}
			}
		}
		if(k==numEscudos)
			++i;
	}
}

//Cria balas e move balas
bool Jogo::naveDispara(int value){

	if(value == 1 && n!=0){ //Caso tenha sido carregada a tecla para disparar é criada uma nova bala...
		if(balasNave.empty() || balasNave.back()->getY() > balaLimiteInf){//...apenas se a lista estiver vazia ou a última bala tenha ultrapassado um certo limite
			Bala *bala = new Bala(n->getX(), n->getY()+0.5, n->getZ());
			balasNave.push_back(bala);
		}
		return false;//Paramos aqui a função caso tenha sido disparada uma nova bala, pois a função balaTimer no main é que está encarregue de mover as balas.
	}

	std::list<Bala*>::iterator i;


	//movem-se todas as balas
	for(i=balasNave.begin(); i != balasNave.end(); ++i){
		(*i)->moveY(0.3);
	}


	//elimina-se a bala que tenha ultrapassado o limite superior
	for(i=balasNave.begin(); i != balasNave.end();){
		if((*i)->getY() > balaLimiteSup){
			i = balasNave.erase(i);
		}
		else
			++i;
	}

	colisaoBalasComEscudo(balasNave);
	bool resultado = colisaoBalasComAliens();
	if(resultado)
		return true;
	return false;
}

//Esta função é usada quando as balas se movem para verificar se houve colisão
bool Jogo::colisaoBalasComAliens(){
	//Verifica se alguma bala atingiu algum alien
	//coordenadas alien
	float aX;
	float aY;
	float aZ;
	float aR;
	//coordenadas bala
	float bX;
	float bY;
	float bZ;
	float d2;
	int k=0, j=0;
	std::list<Bala*>::iterator i;
	for(i=balasNave.begin(); i != balasNave.end(); ++i){
		for(k=0, j=0; !(k==(numLinhas-1) && j==numColunas); j++){
			if(j==numColunas){
				k++;
				j=0;
			}
			if(a[k][j]!=0){
				aX = a[k][j]->getX();
				aY = a[k][j]->getY();
				aZ = a[k][j]->getZ();
				aR = a[k][j]->getRadius();
				bX = (*i)->getX();
				bY = (*i)->getY();
				bZ = (*i)->getZ();
				d2 = ((aX - bX) * (aX - bX)) + ((aY - bY) * (aY - bY)) + ((aZ - bZ) * (aZ - bZ));
				if (d2 <= aR*aR + (*i)->getRadius()){
					i = balasNave.erase(i);
					delete a[k][j];
					a[k][j] = 0;
					return true;
				}
			}
		}
	}
	return false;
}

//Implementação do método draw() da classe Jogo que desenha todos os objectos.
void Jogo::draw(bool wireframe){
	//Desenha as balas da nave
	std::list<Bala*>::iterator i;
	for(i=balasNave.begin(); i != balasNave.end(); ++i)
		(*i)->draw(wireframe);
	//Desenha as balas dos aliens
	for(i=balasAliens.begin(); i != balasAliens.end(); ++i)
		(*i)->drawBalaAlien(wireframe);
	//Desenha Nave
	if(n!=0)
		n->draw(wireframe);
	//Desenha Escudos *4
	for(int i=0; i<numEscudos; i++){
		if(e[i]!=0)
			e[i]->draw(wireframe);
	}
	//Desenha aliens 5*11
	for(int i=0, j=0; !(i==(numLinhas-1) && j==numColunas); j++){
		if(j==numColunas){
			i++;
			j=0;
		}
		if(a[i][j]!=0){
			if(animateAliens)
				a[i][j]->draw(wireframe);
			else
				a[i][j]->drawB(wireframe);
		}
	}
}

//Esta função é usada quando os aliens se movem para baixo para verificar se colidiram com os escudos
void Jogo::colisaoEscudoComAliens(){

	Alien *alien_inf=0;
	//coordenadas alien
	float aX;
	float aY;
	float aZ;
	float aR;
	//coordenadas do escudo
	float eX, eY, eZ;
	float e_big_left, e_big_right, e_big_bot, e_big_top, e_big_near, e_big_far;
	float e_smallL_left, e_smallL_right, e_smallL_bot, e_smallL_top, e_smallL_near, e_smallL_far;
	float e_smallR_left, e_smallR_right, e_smallR_bot, e_smallR_top, e_smallR_near, e_smallR_far;

	//Verifica a linha de baixo
	for(int i=0, j=0; !(i==(numLinhas-1) && j==numColunas); j++) {
		if(j==numColunas && alien_inf != 0){ //Ao chegar à coluna 11 e se houverem aliens nessa linha, o ciclo pára. Pois significa que as colisões foram já verificadas para a linha inferior dos aliens.
			break;
		}
		else if(j==numColunas){//Se não, passa à linha seguinte
			j=0;
			i++;
		}

		if(a[i][j] != 0){
			alien_inf=a[i][j];
			aX = alien_inf->getX();
			aY = alien_inf->getY();
			aZ = alien_inf->getZ();
			aR = alien_inf->getRadius();
			for(int k = 0; k<numEscudos; k++){
				if(e[k]!=0){
					eX = e[k]->getX();
					eY = e[k]->getY();
					eZ = e[k]->getZ();
					//
					e_big_left=e[k]->x_big_left + eX;
					e_big_right=e[k]->x_big_right + eX;
					e_big_bot=e[k]->y_big_bot + eY;
					e_big_top=e[k]->y_big_top + eY;
					e_big_near=e[k]->z_big_near + eZ;
					e_big_far=e[k]->z_big_far + eZ;
					//
					e_smallL_left=e[k]->x_smallL_left + eX;
					e_smallL_right=e[k]->x_smallL_right + eX;
					e_smallL_bot=e[k]->y_smallL_bot + eY;
					e_smallL_top=e[k]->y_smallL_top + eY;
					e_smallL_near=e[k]->z_smallL_near + eZ;
					e_smallL_far=e[k]->z_smallL_far + eZ;
					//
					e_smallR_left=e[k]->x_smallR_left + eX;
					e_smallR_right=e[k]->x_smallR_right + eX;
					e_smallR_bot=e[k]->y_smallR_bot + eY;
					e_smallR_top=e[k]->y_smallR_top + eY;
					e_smallR_near=e[k]->z_smallR_near + eZ;
					e_smallR_far=e[k]->z_smallR_far + eZ;

					if (
						(((aX+aR)>e_big_left && (aX-aR)<e_big_right) && ((aY+aR)>e_big_bot && (aY-aR)<e_big_top) && ((aZ+aR)>e_big_far && (aZ-aR)<e_big_near))

						|| (((aX+aR)>e_smallL_left && (aX-aR)<e_smallL_right) && ((aY+aR)>e_smallL_bot && (aY-aR)<e_smallL_top) && ((aZ+aR)>e_smallL_far && (aZ-aR)<e_smallL_near))

						|| (((aX+aR)>e_smallR_left && (aX-aR)<e_smallR_right) && ((aY+aR)>e_smallR_bot && (aY-aR)<e_smallR_top) && ((aZ+aR)>e_smallR_far && (aZ-aR)<e_smallR_near))
						){
						delete e[k];
						e[k]=0;
						break;
					}
				}
			}
		}
	}
}

bool Jogo::colisaoNaveComAliens(){

	Alien *alien_inf=0;
	//coordenadas alien
	float aX;
	float aY;
	float aZ;
	float aR;
	//coordenadas da nave
	float nX;
	float nY;
	float nZ;
	float nR;
	float d2;

	//Verifica a linha de baixo
	for(int i=0, j=0; !(i==(numLinhas-1) && j==numColunas); j++) {
		if(j==numColunas && alien_inf != 0){ //Ao chegar à coluna 11 e se houverem aliens nessa linha, o ciclo pára. Pois significa que as colisões foram já verificadas para a linha inferior dos aliens.
			break;
		}
		else if(j==numColunas){//Se não, passa à linha seguinte
			j=0;
			i++;
		}

		if(a[i][j] != 0){
			alien_inf=a[i][j];
			aX = alien_inf->getX();
			aY = alien_inf->getY();
			aZ = alien_inf->getZ();
			aR = alien_inf->getRadius();
			if(n!=0){
				nX = n->getX();
				nY = n->getY();
				nZ = n->getZ();
				nR = n->getRadius();
				d2 = ((aX - nX) * (aX - nX)) + ((aY - nY) * (aY - nY)) + ((aZ - nZ) * (aZ - nZ));
				if (d2 <= aR*aR + nR*nR){
					vidas=0;
					return true;
				}
			}
		}
	}
	return false;
}

int Jogo::moveAliens(){

	//Alien da coluna mais à esquerda, alien da coluna mais à direita e alien da linha mais abaixo.
	Alien *alien_esq=0, *alien_dir=0, *alien_inf=0;
	//Limites até onde os aliens se movem.
	float limite_esq=-6.0f, limite_dir=6.0f, limite_inf=-5.0f;
	//Distância que os aliens percorrem em cada unidade de tempo
	float numX= 0.2;
	float numY = -0.2f;

	//Verifica as colunas da esquerda
	for(int i=0, j=0; !(i==numLinhas && j==(numColunas-1)); i++) {
		if(i==numLinhas) { //Ao chegar à linha 5, a linha passa a ser 0, a coluna passa à seguinte.
			i=0;
			j++;
		}
		if(a[i][j]!=0){
			alien_esq=a[i][j];
			break;
		}
	}
	if(alien_esq==0)
		return 3;
	//Verifica as colunas da direita
	for(int i=0, j=(numColunas-1); !(i==numLinhas && j==0); i++) {
		if(i==numLinhas) { //Ao chegar à linha 5, a linha passa a ser 0, a coluna passa à seguinte.
			i=0;
			j--;
		}
		if(a[i][j]!=0){
			alien_dir=a[i][j];
			break;
		}
	}
	//Verifica a linha de baixo
	for(int i=0, j=0; !(i==(numLinhas-1) && j==numColunas); j++) {
		if(j==numColunas) { //Ao chegar à coluna 11, a coluna passa a ser 0, a linha passa à seguinte.
			j=0;
			i++;
		}
		if(a[i][j]!=0){
			alien_inf=a[i][j];
			break;
		}
	}
	//Os aliens movem para a direita/esquerda dentro dos limites, caso tenham acabado de descer passam a mover-se no sentido contrário.
	if((alien_esq->getX() > limite_esq && alien_dir->getX() < limite_dir) || dirAliens == 2 || dirAliens == -2){
		if(dirAliens == 2)
			dirAliens=1;
		else if (dirAliens == -2)
			dirAliens=-1;

		if(dirAliens<0)
			numX*=-1.0f;
		
		for(int i=0, j=0; !(i==(numLinhas-1) && j==numColunas); j++){
			if(j==numColunas){
				i++;
				j=0;
			}
			if(a[i][j]!=0)
				a[i][j]->moveX(numX);
		}
	}
	//Assim que os aliens atingem um dos limites movem-se para baixo.
	else if(alien_esq->getX() <= limite_esq || alien_dir->getX() >= limite_dir)
	{
		if(alien_inf->getY() < limite_inf)
			return 1;//Para indicar que os aliens chegaram ao limite inferior da janela e um novo jogo tem que ser criado.
		if(alien_esq->getX() <= limite_esq)
			dirAliens=2;//Caso seja o limite esquerdo, a variável passa a indicar que os aliens têm que se mover para a direita no próximo instante.
		else
			dirAliens=-2;//Caso seja o limite direito, a variável passa a indicar que os aliens têm que se mover para a esquerda no próximo instante.

		for(int i=0, j=0; !(i==(numLinhas-1) && j==numColunas); j++){
			if(j==numColunas){
				i++;
				j=0;
			}
			if(a[i][j]!=0)
				a[i][j]->moveY(numY);
		}
		animateAliens=!animateAliens;
		colisaoEscudoComAliens();
		int verifica = colisaoNaveComAliens();
		if(verifica==1)//Para indicar que a Nave colidiu com os aliens
			return 1;


		return 2;//Para indicar que os aliens se passarão a mover mais depressa.
	}
	bool verifica = colisaoNaveComAliens();
	if(verifica)//Para indicar que a Nave colidiu com os aliens
		return 1;
	colisaoEscudoComAliens();
	animateAliens=!animateAliens;
	return 0;
}

