/**
 * Declaração da classe Escudo que herda da classe abstracta Entidade.
 *
 * //Escudo.h
 *
 * @author Grupo 18, Turno 3ª 17:30
 * @author Daniel Freire Lascas - 73824
 * @author David Manuel Sales Goncalves - 73891
 * @author Ricardo Manuel Mota de Moura - 74005
 */


#pragma once//Para evitar incluir esta classe mais que uma vez no processo de compilação, usamos #pragma once (podiamos usar #include guards).

#include "Entidade.h"
#include <windows.h>

#if defined(__APPLE__) || defined(MACOSX)
	#include <GLUT/glut.h>
#else
	#include <GL/glut.h>
#endif

class Escudo : public Entidade {
public:
	float x_big_left, x_big_right, y_big_top, y_big_bot, z_big_near, z_big_far;
	float x_smallL_left, x_smallL_right, y_smallL_top, y_smallL_bot, z_smallL_near, z_smallL_far;
	float x_smallR_left, x_smallR_right, y_smallR_top, y_smallR_bot, z_smallR_near, z_smallR_far;
	Escudo(float x, float y, float z);
	~Escudo();
	void draw(bool wireframe);
	int resistance;

};