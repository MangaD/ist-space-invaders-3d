/**
 * Implementação da classe Camera3DFollow que herda da classe abstracta Camera. Mostra a cena em perspectiva e simula uma vista na 1ª pessoa.
 *
 * //Camera3DFollow.cpp
 *
 * @author Grupo 18, Turno 3ª 17:30
 * @author Daniel Freire Lascas - 73824
 * @author David Manuel Sales Goncalves - 73891
 * @author Ricardo Manuel Mota de Moura - 74005
 */


#include "Camera3DFollow.h"

Camera3DFollow::Camera3DFollow()
	: Camera(){
}

Camera3DFollow *Camera3DFollow::camera = 0;

Camera3DFollow* Camera3DFollow::getCamera(){
	if (!camera)
		camera=new Camera3DFollow();
	return camera;
}


//Implementação do método proj() chamado pela função display para definir a matriz de projecção do desenho que se segue (3D neste caso).
void Camera3DFollow::proj(int width, int height) {

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	double ratio = (double) width/height;	//ratio = dimensões do viewport = w/h.
	gluPerspective(90.0, ratio, 0.1, 20.0);

	Nave *n = Jogo::getJogo().getNave();
	if(n!=0){
		naveX = n->getX();
		naveY = n->getY();//O y é negativo.
		naveZ = n->getZ();
	}

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0.0+naveX, naveY-1.0, 4.0+naveZ,//Posição da câmara, varia com o x (o y e o z se aplicável)
		naveX, -naveY+2.5, 0.0,//Posição da cena, varia com a posição da nave
		0.0, 0.0, 1.0);

	GLfloat posDirecional[4] = {0.0, 0.0, 5.0, 0.0};//direcção da luz - última coordenada especifica se é direcional (0) ou posicional (1)
	glLightfv(GL_LIGHT0, GL_POSITION, posDirecional);

	GLfloat posPosicional[4] = {naveX, naveY+0.5f, naveZ, 1.0};//direcção da luz
	glLightfv(GL_LIGHT1, GL_POSITION, posPosicional);
}