/**
 * Declaração da classe Bala que herda de Entidade.
 *
 * //Bala.h
 *
 * @author Grupo 18, Turno 3ª 17:30
 * @author Daniel Freire Lascas - 73824
 * @author David Manuel Sales Goncalves - 73891
 * @author Ricardo Manuel Mota de Moura - 74005
 */

#pragma once//Para evitar incluir esta classe mais que uma vez no processo de compilação, usamos #pragma once (podiamos usar #include guards).

#include "Entidade.h"
#include <windows.h>

#if defined(__APPLE__) || defined(MACOSX)
	#include <GLUT/glut.h>
#else
	#include <GL/glut.h>
#endif

class Bala: public Entidade {

public:
	Bala(float x, float y, float z);
	~Bala();
	void draw(bool wireframe);
	void drawBalaAlien(bool wireframe);
};