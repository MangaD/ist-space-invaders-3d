/**
 * Implementação da classe Alien_A que herda da classe abstracta Alien.
 *
 * //Alien_A.cpp
 *
 * @author Grupo 18, Turno 3ª 17:30
 * @author Daniel Freire Lascas - 73824
 * @author David Manuel Sales Goncalves - 73891
 * @author Ricardo Manuel Mota de Moura - 74005
 */

#include "Alien_A.h"
#include "Jogo.h"

//Construtor da classe Alien
Alien_A::Alien_A(float x, float y, float z) 
	: Alien(x,y,z)  // O construtor de uma subclasse tem sempre que chamar o construtor da classe acima na hierarquia.
{ 
	radius = 0.3;
	r=0.0f;
	g=0.8f;
	b=1.0f;
}

Alien_A::~Alien_A(){
	Jogo::getJogo().increaseScore(10);
	Jogo::getJogo().getListaExplosoes().push_back(new ParticleSystem(x, y, z, r, g, b, 13.5, 100));
}


void Alien_A::draw(bool wireframe) {

	glPushMatrix();//Este PushMatrix tal como o PopMatrix no final são necessários para que o draw da classe jogo funcione correctamente no display do main.
	
	glTranslatef(x, y, z);				//Desenha o alien nas coordenadas x, y, z
	glColor4f(r, g, b, 1.0f);
	//Características do material
	GLfloat matAmbient[] = {r, g, b, 1.0f};
	GLfloat matDiffuse[] = {r, g, b, 1.0f};
	GLfloat matSpecular[] = {0.0f, 0.0f, 0.0f, 1.0f};
	GLfloat matEmission[] = {0.0f, 0.0f, 0.0f, 1.0f};
	GLfloat shininess[] = {0.0f};

	glMaterialfv(GL_FRONT, GL_AMBIENT, matAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, matDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, matSpecular);
	glMaterialfv(GL_FRONT, GL_EMISSION, matEmission);
	glMaterialfv(GL_FRONT, GL_SHININESS, shininess);
	//


	//Bounding Sphere
	glPushMatrix();
	if(wireframe){
		glutWireSphere(radius, 10, 10);
	}
	glPopMatrix();
	//


	glScalef(0.05f, 0.05f, 0.2f);		//Reduz o tamanho final do alien para caber no viewport

	//Cria a parte de cima do alien
	glPushMatrix();
	glScalef(2.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(4.5f, 0.0f, 0.0f);
	glScalef(3.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-4.5f, 0.0f, 0.0f);
	glScalef(3.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 1.0f, 0.0f);
	glScalef(12.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, -1.0f, 0.0f);
	glScalef(12.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 2.0f, 0.0f);
	glScalef(10.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 3.0f, 0.0f);
	glScalef(4.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	//Cria a parte de baixo do alien
	glPushMatrix();
	glTranslatef(-2.5f, -2.0f, 0.0f);
	glScalef(3.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(2.5f, -2.0f, 0.0f);
	glScalef(3.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();
	//
	glPushMatrix();
	glTranslatef(0.0f, -3.0f, 0.0f);
	glScalef(2.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(4.0f, -3.0f, 0.0f);
	glScalef(2.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-4.0f, -3.0f, 0.0f);
	glScalef(2.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();
	//
	glPushMatrix();
	glTranslatef(3.0f, -4.0f, 0.0f);
	glScalef(2.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-3.0f, -4.0f, 0.0f);
	glScalef(2.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPopMatrix();

}


void Alien_A::drawB(bool wireframe) {

	glPushMatrix();//Este PushMatrix tal como o PopMatrix no final são necessários para que o draw da classe jogo funcione correctamente no display do main.
	
	glTranslatef(x, y, z);				//Desenha o alien nas coordenadas x, y, z
	glColor4f(r, g, b, 1.0f);
	//Características do material
	GLfloat matAmbient[] = {r, g, b, 1.0f};
	GLfloat matDiffuse[] = {r, g, b, 1.0f};
	GLfloat matSpecular[] = {0.0f, 0.0f, 0.0f, 1.0f};
	GLfloat matEmission[] = {0.0f, 0.0f, 0.0f, 1.0f};
	GLfloat shininess[] = {0.0f};

	glMaterialfv(GL_FRONT, GL_AMBIENT, matAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, matDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, matSpecular);
	glMaterialfv(GL_FRONT, GL_EMISSION, matEmission);
	glMaterialfv(GL_FRONT, GL_SHININESS, shininess);
	//


	//Bounding Sphere
	glPushMatrix();
	if(wireframe){
		glutWireSphere(radius, 10, 10);
	}
	glPopMatrix();
	//


	glScalef(0.05f, 0.05f, 0.2f);		//Reduz o tamanho final do alien para caber no viewport

	//Cria a parte de cima do alien
	glPushMatrix();
	glScalef(2.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(4.5f, 0.0f, 0.0f);
	glScalef(3.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-4.5f, 0.0f, 0.0f);
	glScalef(3.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 1.0f, 0.0f);
	glScalef(12.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, -1.0f, 0.0f);
	glScalef(12.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 2.0f, 0.0f);
	glScalef(10.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 3.0f, 0.0f);
	glScalef(4.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	//Cria a parte de baixo do alien
	glPushMatrix();
	glTranslatef(-2.0f, -2.0f, 0.0f);
	glScalef(2.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(2.0f, -2.0f, 0.0f);
	glScalef(2.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, -3.0f, 0.0f);
	glScalef(2.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(3.0f, -3.0f, 0.0f);
	glScalef(2.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-3.0f, -3.0f, 0.0f);
	glScalef(2.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(5.0f, -4.0f, 0.0f);
	glScalef(2.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-5.0f, -4.0f, 0.0f);
	glScalef(2.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPopMatrix();
}