/**
 * Declaração da classe abstracta Entidade que será herdada pela Nave, Escudo e Alien.
 *
 * //Entidade.h
 *
 * @author Grupo 18, Turno 3ª 17:30
 * @author Daniel Freire Lascas - 73824
 * @author David Manuel Sales Goncalves - 73891
 * @author Ricardo Manuel Mota de Moura - 74005
 */

#pragma once//Para evitar incluir esta classe mais que uma vez no processo de compilação, usamos #pragma once (podiamos usar #include guards).
#include <windows.h>
#if defined(__APPLE__) || defined(MACOSX)
	#include <GLUT/glut.h>
#else
	#include <GL/glut.h>
#endif

class Entidade {
protected: 
	float x, y, z;
	float r, g, b;
	float radius;
//	GLint polygonMode;
public:
	Entidade(float x, float y, float z);
	virtual ~Entidade();
	virtual void draw(bool wireframe) = 0;   /*Ao declarar uma função virtual pura na classe, a classe (e a função) passa a ser considerada abstracta. 
							   Não se pode instanciar um objecto Entidade. A função draw() tem que ser redefinida nas subclasses, caso
							   contrário estas serão também abstractas */
	float getX();
	float getY();
	float getZ();
	float getR();
	float getG();
	float getB();
	float getRadius();
	void moveX(float num);
	void moveY(float num);
	void moveZ(float num);

};//Classes em C++, tal como as estruturas (struct), levam um ';'
