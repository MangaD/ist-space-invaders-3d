
/**
 * Implementação da classe Explosao - Cria sistemas de particulas.
 *
 * //jogo.cpp
 *
 * @author Grupo 18, Turno 3ª 17:30
 * @author Daniel Freire Lascas - 73824
 * @author David Manuel Sales Goncalves - 73891
 * @author Ricardo Manuel Mota de Moura - 74005
 */

#include "Explosion.h"

#include <windows.h>
#include <math.h>
#if defined(__APPLE__) || defined(MACOSX)
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#define frand()			((float)rand()/RAND_MAX)
#ifndef M_PI
#define M_PI 3.14159265
#endif


// Particle implementation

Particle::Particle(float nx, float ny, float nz, float nr, float ng, float nb, float lifeDecay)
{
	elapsedTime=25;
    life = 0.0f;
	Respawn(nx, ny, nz, nr, ng, nb, lifeDecay);
}


const bool Particle::Respawn(float nx, float ny, float nz, float nr, float ng, float nb, float lifeDecay, const bool force)
{
    if ( life <= 0.0f  || force )
    {
        double v = 0.8*frand()+0.2;
        double phi = frand()*M_PI;
        double theta = 2.0*frand()*M_PI;
        x = nx;
        y = ny;
        z = nz;
		life_decay = lifeDecay;
        vx = v * cos(theta) * sin(phi);
        vy = v * cos(phi);
        vz = v * sin(theta) * sin(phi);
        ax =  0.01f; /* just a little wind */
        ay = 0.0f;
        az = -0.15f; /* gravity pull */
     
		//Color
        r = nr;
        g = ng;
        b = nb;
        
        life = 1.0f;		                        /* life granted to particle */
		fade = lifeDecay*(0.0001515 + frand() * .000303);	    /* life decay */
        return true;
    }
    return false;
}


const bool Particle::Update(const bool respawn)
{
    if ( respawn && life <= 0. )
		Respawn(x, y, z, r, g, b, life_decay);
    if ( life > 0. )
    {
        float delta = elapsedTime * .00378;
        x += (vx * delta);
        y += (vy * delta);
        z += (vz * delta);
        vx += (ax * delta);
        vy += (ay * delta);
        vz += (az * delta);
        life -= (fade * elapsedTime );
        return true;
    }
    return false;
}


unsigned int Particle::ParticleSkeleton;

const Particle & Particle::operator = (const Particle & another)
{
    x  = another.x;
    y  = another.y;
    z  = another.z;
    vx = another.vx;
    vy = another.vy;
    vz = another.vz;
    ax = another.ax;
    ay = another.ay;
    az = another.az;
    r  = another.r;
    g  = another.g;
    b  = another.b;
    life = another.life;
    fade = another.fade;
    
    return *this;
}



void Particle::Draw() const
{
    glPushMatrix();
    glTranslatef( x, y, z);
    glColor4f( r, g, b, life);
	//Características do material
	GLfloat matAmbient[] = {r, g, b, life};
	GLfloat matDiffuse[] = {r, g, b, life};
	GLfloat matSpecular[] = {0.0f, 0.0f, 0.0f, life};
	GLfloat matEmission[] = {0.0f, 0.0f, 0.0f, life};
	GLfloat shininess[] = {0.0f};

	glMaterialfv(GL_FRONT, GL_AMBIENT, matAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, matDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, matSpecular);
	glMaterialfv(GL_FRONT, GL_EMISSION, matEmission);
	glMaterialfv(GL_FRONT, GL_SHININESS, shininess);
	//
	glCallList( ParticleSkeleton);
    glPopMatrix();
}


unsigned int Particle::CreateParticleSkeleton()
{
    ParticleSkeleton = glGenLists( 1);
    glNewList( ParticleSkeleton, GL_COMPILE);
	glScalef(0.05, 0.05, 0.05);
    glutSolidCube(1);
    glEndList();
    return ParticleSkeleton;
}



// ParticleSystem Implementation


ParticleSystem::ParticleSystem(float nx, float ny, float nz, float nr, float ng, float nb, float lifeDecay, int nParticles, const bool burst)
{
	Max_Particles = nParticles;
    for ( int i=0; i<Max_Particles; i++)
		activeParticles.push_back( new Particle(nx, ny, nz, nr, ng, nb, lifeDecay));
    respawnParticles = !burst;
}



bool ParticleSystem::Update()
{
    list<Particle*>::iterator it = activeParticles.begin();
    while ( it != activeParticles.end() ) {
        (*it)->Update( respawnParticles);
		if ( (*it)->IsAlive() )
			it++;
		else
		{
			it = activeParticles.erase( it);
		}
    }
	if(activeParticles.empty())
		return true;
	else
		return false;
}


void ParticleSystem::Draw() const
{
    list<Particle*>::const_iterator it = activeParticles.begin();
    while ( it != activeParticles.end() ) {
        (*it)->Draw();
        it++;
    }
}
