/**
 * Declaração da classe Camera3DFollow que herda da classe abstracta Camera. Mostra a cena em perspectiva e simula uma vista na 1ª pessoa.
 *
 * //Camera3DFollow.h
 *
 * @author Grupo 18, Turno 3ª 17:30
 * @author Daniel Freire Lascas - 73824
 * @author David Manuel Sales Goncalves - 73891
 * @author Ricardo Manuel Mota de Moura - 74005
 */

#pragma once//Para evitar incluir esta classe mais que uma vez no processo de compilação, usamos #pragma once (podiamos usar #include guards).

#include "Camera.h"
#include "Jogo.h"
#include <windows.h>

#if defined(__APPLE__) || defined(MACOSX)
	#include <GLUT/glut.h>
#else
	#include <GL/glut.h>
#endif

class Camera3DFollow: public Camera {
private:
	static Camera3DFollow *camera;
	Camera3DFollow();
	float naveX;
	float naveY;
	float naveZ;
public:
	static Camera3DFollow *getCamera();
	void proj(int width, int height);
};
