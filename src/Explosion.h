


#pragma once

#include <string>
#include <list>
using std::string;
using std::list;


// Particle class **************************************************************

class Particle {
	float life;		// life
	float fade;		// fade
	float life_decay;
	float r, g, b; // colour
	float x, y, z; // position
	float vx, vy, vz; // speed
    float ax, ay, az; // aceleration
    static unsigned int ParticleSkeleton;
	int elapsedTime;
public:
    Particle(float x, float y, float z, float nr, float ng, float nb, float lifeDecay);
    Particle( const Particle & another) { *this = another; }
    const Particle & operator = ( const Particle & another);
    const bool Update(const bool respawn);
    void Draw() const;
    const bool Respawn(float nx, float ny, float nz, float nr, float ng, float nb, float lifeDecay, const bool force = false);
	const bool IsAlive() const { return life>0.f; }
	const bool IsDead()  const { return life<=0.0f; } 

    static unsigned int CreateParticleSkeleton();
};

// Particle system *************************************************************

class ParticleSystem {
    
public:
    ParticleSystem( float nx, float ny, float nz, float nr, float ng, float nb, float lifeDecay, int nParticles, const bool burst = true);
    bool Update();
    void Draw() const;
	const int NoParticles() const { return (int) activeParticles.size(); }
protected:
    list<Particle*> activeParticles, deadParticles;
    bool respawnParticles;
    int Max_Particles;
};