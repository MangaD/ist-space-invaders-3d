/**
 * Implementação da classe Escudo que herda da classe abstracta Entidade.
 *
 * //Escudo.cpp
 *
 * @author Grupo 18, Turno 3ª 17:30
 * @author Daniel Freire Lascas - 73824
 * @author David Manuel Sales Goncalves - 73891
 * @author Ricardo Manuel Mota de Moura - 74005
 */


#include "Escudo.h"

Escudo::Escudo(float x, float y, float z) 
	: Entidade(x,y,z)  // O construtor de uma subclasse tem sempre que chamar o construtor da classe acima na hierarquia.
{ 
	radius=0.7f;
	r = (float) 184/255;
	g = (float) 184/255; 
	b = (float) 11/255;
	resistance = 20;
	//Big Square
	x_big_right=0.6f;
	x_big_left=-0.6f;
	y_big_top=0.4f;
	y_big_bot=-0.25f;
	z_big_far=-0.4f;
	z_big_near=0.4f;
	//Small Left Square
	x_smallL_right=-0.28f;
	x_smallL_left=-0.6f;
	y_smallL_top=0.4f;
	y_smallL_bot=-0.5f;
	z_smallL_far=-0.4f;
	z_smallL_near=0.4f;
	//Small Right Square
	x_smallR_right=0.6f;
	x_smallR_left=0.28f;
	y_smallR_top=0.4f;
	y_smallR_bot=-0.5f;
	z_smallR_far=-0.4f;
	z_smallR_near=0.4f;

}

Escudo::~Escudo()
{}

void Escudo::draw(bool wireframe) {
	
	glPushMatrix();//Este PushMatrix tal como o PopMatrix no final são necessários para que o draw da classe jogo funcione correctamente no display do main.
	
	glTranslatef(x, y, z);//Desenha o escudo nas coordenadas x, y, z

	//Bounding Box
	glPushMatrix();
	if(wireframe){
		glColor4f(1, 1, 1, 1);
		//Características do material
		GLfloat matAmbient_box[] = {1, 1, 1, 1};//componente cor objecto sem iluminação
		GLfloat matDiffuse_box[] = {1, 1, 1, 1};//componente cor objecto iluminado
		GLfloat matSpecular_box[] = {0.0f, 0.0f, 0.0f, 1.0f};//componente aspecto metálico
		GLfloat matEmission_box[] = {0.0f, 0.0f, 0.0f, 1.0f};//componente da luz emitida pelo objecto
		GLfloat shininess_box[] = {0.0f};

		glMaterialfv(GL_FRONT, GL_AMBIENT, matAmbient_box);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, matDiffuse_box);
		glMaterialfv(GL_FRONT, GL_SPECULAR, matSpecular_box);
		glMaterialfv(GL_FRONT, GL_EMISSION, matEmission_box);
		glMaterialfv(GL_FRONT, GL_SHININESS, shininess_box);
		//
		glPushMatrix();
		glTranslatef((x_big_right+x_big_left)/2, (y_big_top+y_big_bot)/2, (z_big_near+z_big_far)/2);
		glScalef(x_big_right-x_big_left, y_big_top-y_big_bot, z_big_near-z_big_far);
		glutWireCube(1.0f);
		glPopMatrix();
		//
		glPushMatrix();
		glTranslatef((x_smallL_right+x_smallL_left)/2, (y_smallL_top+y_smallL_bot)/2, (z_smallL_near+z_smallL_far)/2);
		glScalef(x_smallL_right-x_smallL_left, y_smallL_top-y_smallL_bot, z_smallL_near-z_smallL_far);
		glutWireCube(1.0f);
		glPopMatrix();
		//
		glPushMatrix();
		glTranslatef((x_smallR_right+x_smallR_left)/2, (y_smallR_top+y_smallR_bot)/2, (z_smallR_near+z_smallR_far)/2);
		glScalef(x_smallR_right-x_smallR_left, y_smallR_top-y_smallR_bot, z_smallR_near-z_smallR_far);
		glutWireCube(1.0f);
		glPopMatrix();
		//
	}
	glPopMatrix();
	//

	glColor4f(r, g, b, (float)resistance/20);//(184,134,11)
	//Características do material
	GLfloat matAmbient[] = {0.65164f, 0.50648f, 0.06648f, (float)resistance/20};//componente cor objecto sem iluminação
	GLfloat matDiffuse[] = {0.35164f, 0.50648f, 0.06648f, (float)resistance/20};//componente cor objecto iluminado
	GLfloat matSpecular[] = {0.0f, 0.0f, 0.0f, 1.0f};//componente aspecto metálico
	GLfloat matEmission[] = {0.0f, 0.0f, 0.0f, 1.0f};//componente da luz emitida pelo objecto
	GLfloat shininess[] = {0.0f};

	glMaterialfv(GL_FRONT, GL_AMBIENT, matAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, matDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, matSpecular);
	glMaterialfv(GL_FRONT, GL_EMISSION, matEmission);
	glMaterialfv(GL_FRONT, GL_SHININESS, shininess);
	//


	glTranslatef(0, 0.2, 0);
	glScalef(0.05f, 0.05f, 0.8f);//Reduz o tamanho final do escudo para caber no viewport
	glScalef(25.0f, 1.0f, 1.0f);

	glPushMatrix();
	glTranslatef(0.0f, 0.0f, 0.0f);
	glScalef(1.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	//Parte superior do escudo.
	glPushMatrix();
	glTranslatef(0.0f, 1.0f, 0.0f);
	glScalef(0.9f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 2.0f, 0.0f);
	glScalef(0.8f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 3.0f, 0.0f);
	glScalef(0.7f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 4.0f, 0.0f);
	glScalef(0.6f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 5.0f, 0.0f);
	glScalef(0.5f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	//Parte inferior do escudo

	glPushMatrix();
	glTranslatef(0.0f, -4.5f, 0.0f);
	glScalef(1.0f, 8.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-0.325f, -9.0f, 0.0f);
	glScalef(0.35f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.325f, -9.0f, 0.0f);
	glScalef(0.35f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-0.35f, -10.0f, 0.0f);
	glScalef(0.3f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.35f, -10.0f, 0.0f);
	glScalef(0.3f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-0.375f, -11.0f, 0.0f);
	glScalef(0.25f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.375f, -11.0f, 0.0f);
	glScalef(0.25f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();
	
	glPushMatrix();
	glTranslatef(-0.375f, -12.0f, 0.0f);
	glScalef(0.25f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.375f, -12.0f, 0.0f);
	glScalef(0.25f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();
	
	glPushMatrix();
	glTranslatef(-0.375f, -13.0f, 0.0f);
	glScalef(0.25f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.375f, -13.0f, 0.0f);
	glScalef(0.25f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-0.375f, -14.0f, 0.0f);
	glScalef(0.25f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.375f, -14.0f, 0.0f);
	glScalef(0.25f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPopMatrix();
}