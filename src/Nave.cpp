/**
 * Implementação da classe Nave que herda da classe abstracta Entidade.
 *
 * //Nave.cpp
 *
 * @author Grupo 18, Turno 3ª 17:30
 * @author Daniel Freire Lascas - 73824
 * @author David Manuel Sales Goncalves - 73891
 * @author Ricardo Manuel Mota de Moura - 74005
 */


#include "Nave.h"
#include "Jogo.h"

Nave::Nave(float x, float y, float z) 
	: Entidade(x,y,z) // O construtor de uma subclasse tem sempre que chamar o construtor da classe acima na hierarquia.
{
	radius=0.5;
	r=(float)192/255;
	g= (float)192/255;
	b=(float)192/255;
}

Nave::~Nave(){
	Jogo::getJogo().decreaseVidas(1);
	Jogo::getJogo().getListaExplosoes().push_back(new ParticleSystem(x, y, z, r, g, b, 0.1, 1000));
}

//Implementação do método draw() da classe Nave que desenha a nave.
void Nave::draw(bool wireframe) {

	glPushMatrix();//Este PushMatrix tal como o PopMatrix no final são necessários para que o draw da classe jogo funcione correctamente no display do main.

	glTranslatef(x, y, z);	//Desenha a nave nas coordenadas x, y, z
	glColor4f(r, g, b, 1.0f);

	//Características do material
	GLfloat matAmbient[] = {0.19225f, 0.19225f, 0.19225f, 1.0f};//componente cor objecto sem iluminação
	GLfloat matDiffuse[] = {0.50754f, 0.50754f, 0.50754f, 1.0f};//componente cor objecto iluminado
	GLfloat matSpecular[] = {0.408273f, 0.408273f, 0.408273f, 1.0f};//componente aspecto metálico
	GLfloat matEmission[] = {0.0f, 0.0f, 0.0f, 1.0f};//componente da luz emitida pelo objecto
	GLfloat shininess[] = {1.0f};

	glMaterialfv(GL_FRONT, GL_AMBIENT, matAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, matDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, matSpecular);
	glMaterialfv(GL_FRONT, GL_EMISSION, matEmission);
	glMaterialfv(GL_FRONT, GL_SHININESS, shininess);
	//

	//Bounding Sphere
	glPushMatrix();
	if(wireframe){
		glutWireSphere(radius, 10, 10);
	}
	glPopMatrix();
	//

	/*glTranslatef(x, y, z);
	glScalef(0.4f, 0.4f, 0.4f);

	glNormal3f(0.0f, 0.0f, 1.0f);

	glPushMatrix();

	glScalef(1.4f, 0.6f, 1.0f);
	glBegin(GL_TRIANGLE_FAN);
	glVertex3f (0.0f, 0.0f, 1.0f);
	glVertex3f (0.0f, 1.0f, 0.0f);
	glVertex3f (1.0f, 0.0f, 0.0f);
	glVertex3f (0.0f, -1.0f, 0.0f);
	glVertex3f (-1.0f, 0.0f, 0.0f);
	glVertex3f (0.0f, 1.0f, 0.0f);
	glVertex3f (0.0f, 0.0f, 1.0f);
	glEnd();

	glPopMatrix();

	glTranslatef(0.0f, 0.5f, 0.0f);
	glScalef(0.6f, 0.6f, 0.6f);
	glBegin(GL_TRIANGLE_FAN);
	glVertex3f (0.0f, 0.0f, 1.0f);
	glVertex3f (0.0f, 1.0f, 0.0f);
	glVertex3f (1.0f, 0.0f, 0.0f);
	glVertex3f (0.0f, -1.0f, 0.0f);
	glVertex3f (-1.0f, 0.0f, 0.0f);
	glVertex3f (0.0f, 1.0f, 0.0f);
	glVertex3f (0.0f, 0.0f, 1.0f);
	glEnd();*/
	glTranslatef(0.0f, -0.15f, 0.0f);
	glScalef(1.0f, 1.0f, 1.0f);	//Reduz o tamanho final da nave para caber no viewport
	glScalef(1.0f, 0.5f, 0.5f);
	//Pentágono com raio=0.6
	glPushMatrix();
	glScalef(1.0f, 1.0f, 1.0f);
	glTranslatef(0.0f, 0.0f ,0.0f);
	glutSolidSphere(0.6, 5, 5);
	glPopMatrix();
	//Pentágono com raio=0.6
	glPushMatrix();
	glTranslatef(0.0f, 0.5f, 0.0f);
	glScalef(0.5f, 1.5f, 0.5f);
	glutSolidSphere(0.6, 5, 5);
	glPopMatrix();

	glPopMatrix();
}
//Implementação do método move() da classe Nave que move a nave segundo o 'x' de acordo com o número introduzido. Se receber positivo anda para a direita, caso contrário anda para a esquerda.
void Nave::move(int i) {

	float num = 0.2f*i;
	float limite = 5.5f;

	if(x > -limite && x < limite) // Limites até onde se pode mover a nave.
		x+=num;
	else if (x<=-limite && i>0)//Caso a nave esteja num dos limites apenas se pode mover para o lado oposto.
		x+=num;
	else if (x>=limite && i<0)
		x+=num;

}