/**
 * Declaração da classe Jogo (singleton) - Cria todos os objectos a serem desenhados.
 *
 * //jogo.h
 *
 * @author Grupo 18, Turno 3ª 17:30
 * @author Daniel Freire Lascas - 73824
 * @author David Manuel Sales Goncalves - 73891
 * @author Ricardo Manuel Mota de Moura - 74005
 */

#pragma once//Para evitar incluir esta classe mais que uma vez no processo de compilação, usamos #pragma once (podiamos usar #include guards).
#include <windows.h>
#include "Nave.h"
#include "Escudo.h"
#include "Alien_A.h"
#include "Alien_B.h"
#include "Alien_C.h"
#include "Bala.h"
#include "Explosion.h"
#include <list>
#include <math.h>

class Jogo{

private:
	static Jogo *jogo;//A variável é inicializada a 0.
	Nave *n;
	static const int numLinhas=5, numColunas=11, numEscudos=4;
	float posAliens;
	Escudo *e[numEscudos];//4 escudos.
	Alien *a[numLinhas][numColunas];//5*11 aliens
	short dirAliens;
	bool animateAliens;
	Jogo();//Construtor privado;
	float balaLimiteInf, balaLimiteSup;
	std::list<Bala*> balasNave;
	std::list<Bala*> balasAliens;
	unsigned int score;
	int vidas;
	std::list<ParticleSystem*> listaExplosoes;

public:
	int moveAliens();
	void draw(bool wireframe);
	bool naveDispara(int value);
	bool alienDispara(int value);
	static Jogo& getJogo(); // O '&' significa que a função devolve o endereço do conteúdo.
	Nave* getNave(); //O '*' significa que a função devolve um ponteiro.
	void setNave(Nave* nave);
	std::list<ParticleSystem*> &getListaExplosoes();
	unsigned int getScore();
	int getVidas();
	static void novoJogo();
	void nextLevel();
	void increaseScore(unsigned int value);
	void decreaseVidas(unsigned int value);
	bool colisaoBalasComAliens();//Retorna 1 se houver colisão
	bool colisaoBalasComNave();
	void colisaoBalasComEscudo(std::list<Bala*> &lista);
	void colisaoEscudoComAliens();//Retorna 1 se houver colisão
	bool colisaoNaveComAliens();
};