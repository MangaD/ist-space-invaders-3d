/**
 * Implementação das funções da classe abstracta Entidade que será herdada pela Nave, Escudo e Alien.
 *
 * //Entidade.cpp
 *
 * @author Grupo 18, Turno 3ª 17:30
 * @author Daniel Freire Lascas - 73824
 * @author David Manuel Sales Goncalves - 73891
 * @author Ricardo Manuel Mota de Moura - 74005
 */

#include "Entidade.h"

//Construtor da classe Entidade
Entidade::Entidade(float a, float b, float c) {
		x = a;
		y = b;
		z = c;
	}

Entidade::~Entidade()
{}

float Entidade::getX(){
	return x;
}

float Entidade::getY(){
	return y;
}

float Entidade::getZ(){
	return z;
}

float Entidade::getR(){
	return r;
}

float Entidade::getG(){
	return g;
}

float Entidade::getB(){
	return b;
}

float Entidade::getRadius(){
	return radius;
}

void Entidade::moveX(float num){
	x+=num;
}

void Entidade::moveY(float num){
	y+=num;
}

void Entidade::moveZ(float num){
	z+=num;
}