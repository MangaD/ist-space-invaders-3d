/**
 * Declaração da classe Camera2D que herda da classe abstracta Camera. Mostra a cena em 2D.
 *
 * //Camera2D.h
 *
 * @author Grupo 18, Turno 3ª 17:30
 * @author Daniel Freire Lascas - 73824
 * @author David Manuel Sales Goncalves - 73891
 * @author Ricardo Manuel Mota de Moura - 74005
 */

#pragma once//Para evitar incluir esta classe mais que uma vez no processo de compilação, usamos #pragma once (podiamos usar #include guards).

#include "Camera.h"
#include <windows.h>

#if defined(__APPLE__) || defined(MACOSX)
	#include <GLUT/glut.h>
#else
	#include <GL/glut.h>
#endif

class Camera2D: public Camera {
private:
	static Camera2D *camera;
	Camera2D();
public:
	static Camera2D *getCamera();
	void proj(int width, int height);
};
