/**
 * Declaração da classe abstracta Alien que herda de Entidade e será herdada pelos diferentes Aliens.
 *
 * //Alien.h
 *
 * @author Grupo 18, Turno 3ª 17:30
 * @author Daniel Freire Lascas - 73824
 * @author David Manuel Sales Goncalves - 73891
 * @author Ricardo Manuel Mota de Moura - 74005
 */

#pragma once//Para evitar incluir esta classe mais que uma vez no processo de compilação, usamos #pragma once (podiamos usar #include guards).

#include "Entidade.h"

class Alien: public Entidade {

public:
	Alien(float x, float y, float z);
	virtual ~Alien();
	virtual void drawB(bool wireframe) = 0;

};