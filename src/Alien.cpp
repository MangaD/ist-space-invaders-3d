/**
 * Implementação dos métodos da classe abstracta Alien que herda de Entidade e será herdada pelos diferentes Aliens.
 *
 * //Alien.cpp
 *
 * @author Grupo 18, Turno 3ª 17:30
 * @author Daniel Freire Lascas - 73824
 * @author David Manuel Sales Goncalves - 73891
 * @author Ricardo Manuel Mota de Moura - 74005
 */

#include "Alien.h"
#include "Jogo.h"

//Construtor da classe Entidade
Alien::Alien(float x, float y, float z)
	: Entidade(x, y, z)
{}

Alien::~Alien()
{}
