/**
 * Implementação dos métodos da classe Bala que herda de Entidade.
 *
 * //Bala.cpp
 *
 * @author Grupo 18, Turno 3ª 17:30
 * @author Daniel Freire Lascas - 73824
 * @author David Manuel Sales Goncalves - 73891
 * @author Ricardo Manuel Mota de Moura - 74005
 */

#include "Bala.h"

//Construtor da classe Entidade
Bala::Bala(float x, float y, float z)
	: Entidade(x, y, z)
{ 
	radius = 0.1;
}

Bala::~Bala()
{}

void Bala::draw(bool wireframe){
	glPushMatrix();

	glTranslatef(x, y, z);
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

	//Características do material
	GLfloat matAmbient[] = {0.5f, 0.5f, 0.5f, 1.0f};
	GLfloat matDiffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
	GLfloat matSpecular[] = {1.0f, 1.0f, 1.0f, 1.0f};
	GLfloat shininess[] = {80.0f};

	glMaterialfv(GL_FRONT, GL_AMBIENT, matAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, matDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, matSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, shininess);
	//

	//Bounding Sphere
	glPushMatrix();
	if(wireframe){
		glutWireSphere(radius, 10, 10);
	}
	glPopMatrix();
	//

	glScalef(0.1f, 0.3f, 0.3f);

	glPushMatrix();
	glutSolidCone(1, 1, 5, 5);
	glPopMatrix();

	glPopMatrix();
}

void Bala::drawBalaAlien(bool wireframe){
	glPushMatrix();

	glTranslatef(x, y, z);
	glColor3f(1, 0.5, 0);
	//Características do material
	GLfloat matAmbient[] = {1.0f, 0.1f, 0.0f, 1.0f};
	GLfloat matDiffuse[] = {1.0f, 0.1f, 0.0f, 1.0f};
	GLfloat matSpecular[] = {1.0f, 0.0f, 0.0f, 1.0f};
	GLfloat shininess[] = {80.0f};

	glMaterialfv(GL_FRONT, GL_AMBIENT, matAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, matDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, matSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, shininess);
	//

	//Bounding Sphere
	glPushMatrix();
	if(wireframe){
		glutWireSphere(radius, 10, 10);
	}
	glPopMatrix();
	//

	glScalef(0.07f, 0.3f, 0.1f);

	glPushMatrix();
	glRotatef(180, 0, 0, 1);
	glutSolidCone(1, 1, 5, 5);
	glPopMatrix();

	glPopMatrix();
}

