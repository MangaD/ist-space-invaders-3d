/**
 * Implementação das funções da classe abstracta Camera que será herdada pela Camera2D, Camera3D e Camera3DFollow.
 *
 * //Camera.cpp
 *
 * @author Grupo 18, Turno 3ª 17:30
 * @author Daniel Freire Lascas - 73824
 * @author David Manuel Sales Goncalves - 73891
 * @author Ricardo Manuel Mota de Moura - 74005
 */

#include "Camera.h"

