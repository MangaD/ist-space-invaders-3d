/**
 * Implementação da classe Alien_B que herda da classe abstracta Alien.
 *
 * //Alien_B.cpp
 *
 * @author Grupo 18, Turno 3ª 17:30
 * @author Daniel Freire Lascas - 73824
 * @author David Manuel Sales Goncalves - 73891
 * @author Ricardo Manuel Mota de Moura - 74005
 */

#include "Alien_B.h"
#include "Jogo.h"

//Construtor da classe Alien
Alien_B::Alien_B(float x, float y, float z) 
	: Alien(x,y,z)  // O construtor de uma subclasse tem sempre que chamar o construtor da classe acima na hierarquia.
{ 
	radius = 0.3;
	r=0.0f;
	g=1.0f;
	b=0.0f;
}

Alien_B::~Alien_B(){
	Jogo::getJogo().increaseScore(20);
	Jogo::getJogo().getListaExplosoes().push_back(new ParticleSystem(x, y, z, r, g, b, 13.5, 100));
}


void Alien_B::draw(bool wireframe){
	glPushMatrix();

	glTranslatef(x, y, z);				//Desenha o alien nas coordenadas x, y, z
	glColor4f(r, g, b, 1.0f);

	//Características do material
	GLfloat matAmbient[] = {r, g, b, 1.0f};
	GLfloat matDiffuse[] = {r, g, b, 1.0f};
	GLfloat matSpecular[] = {0.0f, 0.0f, 0.0f, 1.0f};
	GLfloat matEmission[] = {0.0f, 0.0f, 0.0f, 1.0f};
	GLfloat shininess[] = {0.0f};

	glMaterialfv(GL_FRONT, GL_AMBIENT, matAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, matDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, matSpecular);
	glMaterialfv(GL_FRONT, GL_EMISSION, matEmission);
	glMaterialfv(GL_FRONT, GL_SHININESS, shininess);
	//

	//Bounding Sphere
	glPushMatrix();
	if(wireframe){
		glutWireSphere(radius, 10, 10);
	}
	glPopMatrix();
	//

	glScalef(0.05f, 0.05f, 0.2f);		//Reduz o tamanho final do alien para caber no viewport

	//Parte inferior do Alien
	glPushMatrix();
	glScalef(9.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, -1.0f, 0.0f);
	glScalef(7.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();
	
	glPushMatrix();
	glTranslatef(-3.0f, -2.0f, 0.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(3.0f, -2.0f, 0.0f);
	glutSolidCube(1);
	glPopMatrix();
	
	glPushMatrix();
	glTranslatef(-1.5f, -3.0f, 0.0f);
	glScalef(2.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(1.5f, -3.0f, 0.0f);
	glScalef(2.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();
	//
	glPushMatrix();
	glTranslatef(-5.0f, 2.0f, 0.0f);
	glScalef(1.0f, 3.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(5.0f, 2.0f, 0.0f);
	glScalef(1.0f, 3.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();
	//
	//Parte superior do Alien
	glPushMatrix();
	glTranslatef(0.0f, 1.0f, 0.0f);
	glScalef(3.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-3.5f, 1.0f, 0.0f);
	glScalef(2.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(3.5f, 1.0f, 0.0f);
	glScalef(2.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 2.0f, 0.0f);
	glScalef(7.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-2.0f, 3.0f, 0.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-3.0f, 4.0f, 0.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(2.0f, 3.0f, 0.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(3.0f, 4.0f, 0.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPopMatrix();
}


void Alien_B::drawB(bool wireframe) {
	glPushMatrix();

	glTranslatef(x, y, z);				//Desenha o alien nas coordenadas x, y, z
	glColor4f(r, g, b, 1.0f);

	//Características do material
	GLfloat matAmbient[] = {r, g, b, 1.0f};
	GLfloat matDiffuse[] = {r, g, b, 1.0f};
	GLfloat matSpecular[] = {0.0f, 0.0f, 0.0f, 1.0f};
	GLfloat matEmission[] = {0.0f, 0.0f, 0.0f, 1.0f};
	GLfloat shininess[] = {0.0f};

	glMaterialfv(GL_FRONT, GL_AMBIENT, matAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, matDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, matSpecular);
	glMaterialfv(GL_FRONT, GL_EMISSION, matEmission);
	glMaterialfv(GL_FRONT, GL_SHININESS, shininess);
	//

	//Bounding Sphere
	glPushMatrix();
	if(wireframe){
		glutWireSphere(radius, 10, 10);
	}
	glPopMatrix();
	//

	glScalef(0.05f, 0.05f, 0.2f);		//Reduz o tamanho final do alien para caber no viewport

	//Parte inferior do Alien
	glPushMatrix();
	glScalef(11.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, -1.0f, 0.0f);
	glScalef(7.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();
	
	glPushMatrix();
	glTranslatef(-3.0f, -2.0f, 0.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(3.0f, -2.0f, 0.0f);
	glutSolidCube(1);
	glPopMatrix();
	
	glPushMatrix();
	glTranslatef(-1.5f, -3.0f, 0.0f);
	glScalef(2.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(1.5f, -3.0f, 0.0f);
	glScalef(2.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();
	//
	glPushMatrix();
	glTranslatef(-5.0f, -1.5f, 0.0f);
	glScalef(1.0f, 2.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(5.0f, -1.5f, 0.0f);
	glScalef(1.0f, 2.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();
	//
	//Parte superior do Alien
	glPushMatrix();
	glTranslatef(0.0f, 1.0f, 0.0f);
	glScalef(3.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-3.5f, 1.0f, 0.0f);
	glScalef(2.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(3.5f, 1.0f, 0.0f);
	glScalef(2.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 2.0f, 0.0f);
	glScalef(7.0f, 1.0f, 1.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-2.0f, 3.0f, 0.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-3.0f, 4.0f, 0.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(2.0f, 3.0f, 0.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(3.0f, 4.0f, 0.0f);
	glutSolidCube(1);
	glPopMatrix();

	glPopMatrix();
}