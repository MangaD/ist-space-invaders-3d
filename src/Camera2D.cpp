/**
 * Implementação da classe Camera2D que herda da classe abstracta Camera. Mostra a cena em 2D.
 *
 * //Camera2D.cpp
 *
 * @author Grupo 18, Turno 3ª 17:30
 * @author Daniel Freire Lascas - 73824
 * @author David Manuel Sales Goncalves - 73891
 * @author Ricardo Manuel Mota de Moura - 74005
 */


#include "Camera2D.h"

Camera2D::Camera2D()
	: Camera() {}

Camera2D *Camera2D::camera = 0;

Camera2D* Camera2D::getCamera(){
	if (!camera)
		camera=new Camera2D();
	return camera;
}


//Implementação do método proj() chamado pela função display para definir a matriz de projecção do desenho que se segue (2D neste caso).
void Camera2D::proj(int width, int height) {

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	float ratio = (float) width/height;	//ratio = w/h. Razão entre a largura e o comprimento do viewport.
	float dimension = 5.5f; // dimensão da projecção ortogonal no viewport.

	if(ratio>1)
		glOrtho(-dimension*ratio, dimension*ratio, -dimension, dimension, -dimension, dimension);//Define o volume de visualização para projecção paralela ortogonal. Por omissão a câmara está na origem. Multiplica a matriz actual por uma transformação de projecção. (left, right, bottom, top, -near, -far).
	else
		glOrtho(-dimension, dimension, -dimension/ratio, dimension/ratio, -dimension, dimension);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

}