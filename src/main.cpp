/**
 * Parte 1 - Cena simples interactiva com Câmara Fixa
 *
 * //main.cpp
 *
 * @author Grupo 18, Turno 3ª 17:30
 * @author Daniel Freire Lascas - 73824
 * @author David Manuel Sales Goncalves - 73891
 * @author Ricardo Manuel Mota de Moura - 74005
 */

#include "main.h"

/* Variáveis globais cujo valor é actualizado através da função myReshape(), correspondem às dimensões do viewport */
GLsizei width = 730;
GLsizei height = 600;
//Para saber a posição que o SO dá à janela no momento da criação, permitindo assim repor essa posição quando se sai do modo fullscreen
int screen_pos_x;
int screen_pos_y; 
// Para saber se a janela está em modo fullscreen.
bool fullscreen = false;
// Referência para o objecto da câmera a ser usada
Camera *camera = Camera2D::getCamera();
//tempo usado no glutTimerFunc
int timeAliens = 1000;//1 segundo
int timeBala = 50;
int timeNave = 50;
int timeExplosion = 25;
float fastforward = 1.0f;
//Key buffering - para pressionar multiplas teclas
bool* keyStates = new bool[256]; // Criar um array de booleanos de comprimento 256 (0-255) para incluir todos os caracteres.
bool* keySpecialStates = new bool[256];
//Para saber se está em pausa
bool paused = false;
bool pausedByUser = false;
//Variavel onde ficam guardadas as texturas
GLuint textura;
//
bool help=false;
//
bool wireframe=false;
//
int tempoEspera = 0;
int tempoEsperaNave=3000;
//


int main(int argc, char** argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH|GLUT_DOUBLE|GLUT_RGBA);
	glutInitWindowSize (width, height);
	glutInitWindowPosition (-1, -1);
	glutCreateWindow("Space Invaders - Grupo 18");
	//Para se saber qual a posição da janela a usar quando se sai do fullscreen
	screen_pos_x = glutGet((GLenum)GLUT_WINDOW_X);
	screen_pos_y = glutGet((GLenum)GLUT_WINDOW_Y);
	//
	//Para o Alpha funcionar
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable( GL_BLEND );

	//LIGHTING
	glEnable (GL_DEPTH_TEST);
    glEnable (GL_LIGHT0);
	lightInit();
	//

	PlaySound(TEXT("./star-commander1.wav"), NULL, SND_FILENAME | SND_ASYNC);//Try ensuring that you're passing the right kind of variable, by wrapping the string in TEXT()


	//BACKGROUND
	textura = LoadTexture("./terra.bmp");
	//
	Particle::CreateParticleSkeleton();
	//
	glutDisplayFunc(myDisplay);//Callback functions:
	glutReshapeFunc(myReshape);

	//Key Funcs
	for(int i = 0; i<256;i++){
		keyStates[i] = false;
		keySpecialStates[i] = false;
	}
	//
	glutKeyboardFunc(keyPressed);//Primir teclas normais (esc, por ex.)
	glutKeyboardUpFunc(keyUp);//largar teclas normais
	//
	glutSpecialFunc(keySpecial);//Primir teclas especiais (setas, por ex.)
	glutSpecialUpFunc(keySpecialUp);//Largar teclas especiais (setas, por ex.)
	//

	//Timer Funcs
	glutTimerFunc(timeAliens*fastforward, alienTimer, 0);//Faz o movimento dos aliens
	glutTimerFunc(timeNave*fastforward, naveTimer, 0);//Faz o movimento da nave
	glutTimerFunc(timeBala*fastforward, balaTimer, 0);//Faz o movimento da bala da nave
	glutTimerFunc(timeExplosion*fastforward, explosionsTimer, 0);
	
	//Aliens aleatórios
	srand(time(0));
	int n = rand() % 5000 + 5000;//tempo para a 1ª bala lançada pelo alien entre 5 a 10 segundos.
	glutTimerFunc(n*fastforward, balaAliensTimer, 1);//Faz o movimento da bala dos aliens. Como recebe valor 1 será apenas para criar a bala.
	glutTimerFunc(n*fastforward, balaAliensTimer, 0);//Como recebe o valor 0 inicia o movimento das balas.
	//
	//glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);
	glutMainLoop();
	return 0;
}

void resetTimers(){
	timeAliens = 1000;
	timeBala = 50;
	timeNave = 50;
	timeExplosion = 25;
}

//Esta função é chamada no display
void background()
{
	glEnable(GL_TEXTURE_2D);
	GLfloat matAmb[] = {1.0f, 1.0f, 1.0f, 1.0f};//componente cor objecto sem iluminação
	GLfloat matDif[] = {1.0f, 1.0f, 1.0f, 1.0f};//componente cor objecto iluminado
	GLfloat matSpec[] = {0.0f, 0.0f, 0.0f, 1.0f};//componente aspecto metálico
	GLfloat matEmission[] = {0.0f, 0.0f, 0.0f, 0.0f};//componente da luz emitida pelo objecto
	GLfloat shininess[] = {128.0f};

	glMaterialfv(GL_FRONT, GL_AMBIENT, matAmb);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, matDif);
	glMaterialfv(GL_FRONT, GL_SPECULAR, matSpec);
	glMaterialfv(GL_FRONT, GL_EMISSION, matEmission);
	glMaterialfv(GL_FRONT, GL_SHININESS, shininess);

	glColor3f(0.5f, 0.5f, 0.5f);

	glBindTexture(GL_TEXTURE_2D, textura);

    glPushMatrix();
	glScalef(1.5f, 1.0f, 1.0f);
    glBegin(GL_QUADS);
	glTexCoord2f(0.0, 0.0); glVertex3f(-5.5f, -5.5f, 0.0);
	glTexCoord2f(1.0, 0.0); glVertex3f(5.5f, -5.5f, 0.0);
	glTexCoord2f(1.0, 1.0); glVertex3f(5.5f, 5.5f, 0.0);
	glTexCoord2f(0.0, 1.0); glVertex3f(-5.5f, 5.5f, 0.0);
    glEnd();
    glPopMatrix();

	glDisable(GL_TEXTURE_2D);
}
//Esta função é chamada no main
GLuint LoadTexture (const char * filename){
	
	BMPloader * bitmap = new BMPloader(filename);
	if ( bitmap->Isvalid() )
	{
		glGenTextures(1, &textura);
		glBindTexture(GL_TEXTURE_2D, textura);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, bitmap->Width(), bitmap->Heigth(), 0, GL_RGB, GL_UNSIGNED_BYTE, bitmap->Image());// O GL_RGBA dava problemas.
	}
	delete bitmap;
	return textura;
}

void lightInit(){
	//Iluminação global da cena com fonte de luz direccional.
	GLfloat ambient[] = {0.3, 0.3, 0.3, 1.0};//intensidade da luz ambiente
	GLfloat diffuse[] = {1.0, 1.0, 1.0, 1.0};//intensidade da luz difusa
	GLfloat specular[] = {1.0, 1.0, 1.0, 1.0};//intensidade da luz especular
	glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, specular);
	//
	GLfloat theta[] = {45.0f};
	GLfloat alpha[] = {180.0f};
	glLightfv(GL_LIGHT1, GL_AMBIENT, ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, diffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, specular);
	glLightfv(GL_LIGHT1, GL_SPOT_CUTOFF, theta);
	glLightfv(GL_LIGHT1, GL_SPOT_EXPONENT, alpha);

}

//Função responsável por mover a nave
void naveTimer(int value){
	Nave *n = Jogo::getJogo().getNave();
	if(!paused && !pausedByUser){
		if (!(keySpecialStates[GLUT_KEY_LEFT] && keySpecialStates[GLUT_KEY_RIGHT])){
			if(keySpecialStates[GLUT_KEY_LEFT]){ // Left arrow
				if(n!=0)
					n->move(-1);
				glutPostRedisplay(); // redesenhar tudo para haver mudança.
			}
		
			else if(keySpecialStates[GLUT_KEY_RIGHT]){ // Right arrow
				if(n!=0)
					n->move(1);
				glutPostRedisplay();
			}
		}
	}
	glutTimerFunc(timeNave*fastforward, naveTimer, 0);

}

//Função responsável por mover os aliens
void alienTimer(int value)
{
	if(!paused && !pausedByUser){
	  //Move os aliens como pretendido, a variável 'resultado' serve para indicar como os aliens se movem (mais depressa ou parar).
		int resultado = Jogo::getJogo().moveAliens();
		glutPostRedisplay();// redesenhar tudo para haver mudança.

		if(resultado==2 && (timeAliens-100) > 0)//Caso os aliens cheguem ao limite da janela passam a andar mais depressa.
			timeAliens-=100;
		else if(resultado==3){//Caso os aliens tenham sido todos eliminados
			Jogo::getJogo().nextLevel();
		}
		else if(resultado==1){
			resetTimers();
			Camera3D::getCamera()->proj(width, height);//Faz reset às câmaras com a posição onde a nave rebentou, para o caso de se mudar a vista para uma destas câmaras durante a explosão.
			Camera3DFollow::getCamera()->proj(width, height);
			delete Jogo::getJogo().getNave();
			Jogo::getJogo().setNave(0);
			tempoEspera = glutGet(GLUT_ELAPSED_TIME)+tempoEsperaNave;
			paused=!paused;
			//O explosionsTimer manda criar um novo jogo.
		}
	}
	glutTimerFunc(timeAliens*fastforward, alienTimer, 0);
}

//Função responsável por mover as balas
void balaTimer(int value)
{
	if(!paused && !pausedByUser){
		if(keyStates[' '])
			Jogo::getJogo().naveDispara(1);//Cria bala
		Jogo::getJogo().naveDispara(0);//Move bala

		glutPostRedisplay();
	}
	glutTimerFunc(timeBala*fastforward, balaTimer, 0);
}

void balaAliensTimer(int value)
{
	if(value==0){
		if(!paused && !pausedByUser){
			bool resultado = Jogo::getJogo().alienDispara(0);
			if(resultado){
				Camera3D::getCamera()->proj(width, height);//Faz reset às câmaras com a posição onde a nave rebentou, para o caso de se mudar a vista para uma destas câmaras durante a explosão.
				Camera3DFollow::getCamera()->proj(width, height);
				delete Jogo::getJogo().getNave();
				Jogo::getJogo().setNave(0);
				tempoEspera = glutGet(GLUT_ELAPSED_TIME)+tempoEsperaNave;
				paused=!paused;
			}
			glutPostRedisplay();
		}
		glutTimerFunc(timeBala*fastforward, balaAliensTimer, 0);
	}
	else{
		int n = 0;
		if(!paused && !pausedByUser){
			Jogo::getJogo().alienDispara(1);

			switch(timeAliens){
			case 1000:
				n = rand() % 3000 + 6000;//6 a 9 segundos
				break;
			case 900:
			case 800:
				n = rand() % 3000 + 5000;//5 a 8 segundos
				break;
			case 700:
			case 600:
				n = rand() % 3000 + 4000;//4 a 7 segundos
				break;
			case 500:
			case 400:
				n = rand() % 2000 + 3000;//3 a 5 segundos
				break;
			case 300:
			case 200:
				n = rand() % 1000 + 2000;//2 a 3 segundos
				break;
			case 100:
				n = rand() % 1000 + 1000;//1 a 2 segundos
				break;
			default:
				n=10;
			}
		}
		glutTimerFunc(n*fastforward, balaAliensTimer, 1);
	}
}
//Timer das explosões
void explosionsTimer(const int value)
{
    if (!pausedByUser)
    {
		if((tempoEspera!=0) && (glutGet(GLUT_ELAPSED_TIME) > tempoEspera) && Jogo::getJogo().getNave()==0){
			if(Jogo::getJogo().getVidas() <= 0){
				resetTimers();
				Jogo::novoJogo();
			}
			else{
			Jogo::getJogo().setNave(new Nave(0.0f, -4.0f, 0.0f));
			}
			tempoEspera=0;
			paused=!paused;
		}
		else if (Jogo::getJogo().getNave()!=0 && paused){//Caso se tenha feito reset ao jogo durante a explosão, por exemplo
			tempoEspera=0;
			paused=!paused;
		}
		glutPostRedisplay();
    }
	glutTimerFunc( timeExplosion*fastforward, explosionsTimer, 0);
}








/* Display callback function implementation */
void myDisplay(void) {
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glViewport(0, 0, width, height);//(x, y, w, h).
	
	//Desenha cenas 2D, background e Pause
	draw2Dunder3D(background);
	if(pausedByUser)
		draw2Dover3D(drawPause);
	if(help)
		draw2Dover3D(HDD);
	draw2Dover3D(HUD);
	//Desenha cenas 2D ou 3D, o jogo em si.
	glPushMatrix();
	camera->proj(width, height);
	Jogo::getJogo().draw(wireframe);
	std::list<ParticleSystem*>::iterator i;
	glPushMatrix();
	for(i=Jogo::getJogo().getListaExplosoes().begin(); i != Jogo::getJogo().getListaExplosoes().end();){
		bool resultado = (*i)->Update();
		(*i)->Draw();
		if(resultado)//Caso a explosão tenha terminado
			i = Jogo::getJogo().getListaExplosoes().erase(i);
		else
			++i;
	}
	glPopMatrix();

	glPopMatrix();
	//
	
	glutSwapBuffers();//Usado no Double Buffer
	//glFlush();//Usado no Single Buffer -> glutInitDisplayMode (GLUT_SINGLE);
}



/* Reshape callback function implementation */
void myReshape(GLsizei w, GLsizei h) {
	width = w;
	height = h;
}


/* keySpecial function implementation - Usado na glutSpecialFunc() callback */
void keySpecial(int key, int x, int y) {

	if(keySpecialStates[key] == false){
		keySpecialStates[key] = true;

	}
	if(key == GLUT_KEY_F1){
		if(help==true)
			help=false;
		else
			help=true;
		glutPostRedisplay();
	}
}


void keySpecialUp (int key, int x, int y) {  
	keySpecialStates[key] = false;
} 



void keyUp (unsigned char key, int x, int y) {
	keyStates[key] = false; //Muda o estado da tecla largada para false.
} 


/* keyPressed function implementation - Usado na glutKeybordFunc() callback. */
void keyPressed(unsigned char key, int x, int y) {

	if(keyStates[key] == false){
		keyStates[key] = true; //Muda o estado da tecla primida para true.
		
		switch(key){
		case '0':
			if(fastforward == 1.0f)
				fastforward = 0.1f;//Fastforward.
			else
				fastforward = 1.0f;
		break;

		case '1':
			camera = Camera2D::getCamera();//Camera 2D
			if(glIsEnabled(GL_LIGHTING)){
				glDisable(GL_LIGHTING);
			}
			break;

		case '2':
			camera = Camera3D::getCamera();//Camera 3D atrás da nave
			if(!glIsEnabled(GL_LIGHTING)){
				glEnable(GL_LIGHTING);
			}
			break;

		case '3':
			camera = Camera3DFollow::getCamera();//Camera 3D à frente da nave
			if(!glIsEnabled(GL_LIGHTING)){
				glEnable(GL_LIGHTING);
			}
			break;
		case'4':
			if(glIsEnabled(GL_LIGHT0)){
				glDisable(GL_LIGHT0);
				glEnable(GL_LIGHT1);
			}
			else if(glIsEnabled(GL_LIGHT1))
				glDisable(GL_LIGHT1);
			else
				glEnable(GL_LIGHT0);
			break;
		case '5':
			wireframe=(!wireframe);
			glutPostRedisplay();
			break;
		case 'p':
		case 'P'://Para pausa.
			pausedByUser=!pausedByUser;
			break;

		case 6://Corresponde a CTRL+F - Para fullscreen
			if(!fullscreen){
				glutFullScreen();
				fullscreen = true;
			}
			else{
				glutReshapeWindow(730, 600);
				glutPositionWindow(screen_pos_x, screen_pos_y);//8, 31
				fullscreen=false;
			}
			break;

		case 18://Coresponde a CTRL+R - Para fazer Reset
			resetTimers();
			Jogo::novoJogo();
			break;

		case 27://Corresponde a ESC - para Sair
			exit(0);
	
		}
	
		glutPostRedisplay();
	}


}



// Draw stroke text, at a certain location, scale, color, and BOLD and/or ITALIC
void glutStrokeString (const void * font, char *string, 
					   double translateX, double translateY, double translateZ, 
					   double ScaleX, double ScaleY, double ScaleZ,
					   float Red, float Green, float Blue, 
					   unsigned int Bold, bool Italic){
  // existing line width
  GLfloat lw[1];

  glGetFloatv (GL_LINE_WIDTH, lw);
  if (Bold)
	  glLineWidth(Bold);// bold
  else
	  glLineWidth(1);// normal

  glPushMatrix ();
  glTranslatef (translateX, translateY, translateZ);
  glScalef(ScaleX, ScaleY, ScaleZ);
  if (Italic) 
  {
    // Shear matrix for making italics:
    // (shear in X only)
    // | 1.0  0.5  0.0  0.0 |
    // | 0.0  1.0  0.0  0.0 |
    // | 0.0  0.0  1.0  0.0 |
    // | 0.0  0.0  0.0  1.0 |
    GLfloat M[16] = { 0.0 };
    M[0] = M[5] = M[10] = M[15] = 1.0;
    M[4] = 0.5;
    M[8] = 0.0;
    glMultMatrixf (M);
  }
  glColor3f (Red, Green, Blue);
  for (unsigned int i = 0; i < strlen (string); i++)
    glutStrokeCharacter ((void*) font, string[i]);
  glPopMatrix ();

  // reset line width to what it was
  glLineWidth (lw[0]);
}



void glutBitmapString(void* font, const string &string, double translateX, double translateY)
{
	glRasterPos2d(translateX, translateY);

	for (int i=0; i<(int)string.size(); i++)
	{
		glutBitmapCharacter(font, string[i]);
	}
}

void draw2Dover3D(void (_cdecl *func) (void)){

	int n = 0;
	if(glIsEnabled(GL_LIGHTING)){
		glEnable(GL_DEPTH_TEST);//Para que o fundo fique à frente da cena 3D activamos o Depth
		glDisable(GL_LIGHTING);
		glDepthMask(GL_TRUE);
		n=1;
	}
	glPushMatrix();
	Camera2D::getCamera()->proj(width, height);//Projecção 2D
	func();
	glPopMatrix();
	if(n==1){
		glEnable(GL_LIGHTING);
	}
}

void draw2Dunder3D(void (_cdecl *func) (void)){

	int n = 0;
	if(glIsEnabled(GL_LIGHTING)){
		glDisable(GL_DEPTH_TEST);//Para que o fundo fique atrás da cena 3D desactivamos o Depth
		glDisable(GL_LIGHTING);
		glDepthMask(GL_FALSE);
		n=1;
	}
	glPushMatrix();
	Camera2D::getCamera()->proj(width, height);//Projecção 2D
	func();
	glPopMatrix();
	if(n==1){
		glEnable(GL_LIGHTING);
		glEnable(GL_DEPTH_TEST);
		glDepthMask(GL_TRUE);
	}
}

void drawPause(){
	glPushMatrix();
	glutStrokeString(GLUT_STROKE_ROMAN, (char*)"PAUSE", -4.0, -1.0, 5.0, 0.02, 0.02, 0.02, 1.0, 1.0, 1.0, 4, false);
	glPopMatrix();
}

void HUD(){

	float livesX = 3.5;
	float livesY = 5.0;
	float scoreX = -6.0;
	float scoreY = 5.0;
	//Desenha F1 - Help
	glutStrokeString(GLUT_STROKE_ROMAN, (char*)"F1 - Help", -6.0, -5.3, 5.0, 0.0017, 0.0017, 0.0017, 1.0, 1.0, 1.0, 0, false);
	//Desenha Pontuação
//	std::string s = std::to_string(Jogo::getJogo().getScore());
//	const char *strPontuacao = s.c_str();
	std::stringstream ss;
	ss << Jogo::getJogo().getScore();
	std::string s = ss.str();
	const char *strPontuacao = s.c_str();
	glPushMatrix();
	glutStrokeString(GLUT_STROKE_ROMAN, (char*)"SCORE", scoreX, scoreY, 5.0, 0.002, 0.002, 0.002, 1.0, 1.0, 1.0, 0, false);
	glutStrokeString(GLUT_STROKE_ROMAN, (char*) strPontuacao, scoreX+1.0, scoreY, 5.0, 0.002, 0.002, 0.002, 0.0, 1.0, 0.0, 0, false);
	//Desenha Vidas
	glutStrokeString(GLUT_STROKE_ROMAN, (char*)"LIVES", livesX, livesY, 5.0, 0.002, 0.002, 0.002, 1.0, 1.0, 1.0, 0, false);
	//
	Nave *naveVidas = new Nave(0.0f, 10.2f, 0.0f);
	float nY = 0.0f;
	int k = 0;
	for(int i=0; i<Jogo::getJogo().getVidas(); i++, k++){
		glPushMatrix();
		if(livesX+1.2f + k*0.6 >= livesX+1.2f + 3*0.6){
			k=0;
			nY-=0.7f;
		}
		glTranslatef(livesX+1.2f + k*0.6, nY, 5.0f);
		glScalef(0.5f, 0.5f, 0.5f);
		naveVidas->draw(false);
		glPopMatrix();
	}
	//
	glPopMatrix();
}

void HDD(){
	glPushMatrix();
	glutStrokeString(GLUT_STROKE_ROMAN, (char*)"Ctrl+R - Reset", -6.0, -5.0, 5.0, 0.0017, 0.0017, 0.0017, 0.0, 1.0, 0.0, 0, false);
	glutStrokeString(GLUT_STROKE_ROMAN, (char*)"Ctrl+F - Fullscreen", -6.0, -4.7, 5.0, 0.0017, 0.0017, 0.0017, 0.0, 1.0, 0.0, 0, false);
	glutStrokeString(GLUT_STROKE_ROMAN, (char*)"P - Pause", -1.5, -5.3, 5.0, 0.0017, 0.0017, 0.0017, 0.0, 1.0, 0.0, 0, false);
	glutStrokeString(GLUT_STROKE_ROMAN, (char*)"0 - Fastforward", -1.5, -5.0, 5.0, 0.0017, 0.0017, 0.0017, 0.0, 1.0, 0.0, 0, false);
	glutStrokeString(GLUT_STROKE_ROMAN, (char*)"1, 2, 3 - Change camera view", -1.5, -4.7, 5.0, 0.0017, 0.0017, 0.0017, 0.0, 1.0, 0.0, 0, false);
	glutStrokeString(GLUT_STROKE_ROMAN, (char*)"4 - Change lights", 3.5, -4.7, 5.0, 0.0017, 0.0017, 0.0017, 0.0, 1.0, 0.0, 0, false);
	glutStrokeString(GLUT_STROKE_ROMAN, (char*)"5 - Wireframe", 3.5, -5.0, 5.0, 0.0017, 0.0017, 0.0017, 0.0, 1.0, 0.0, 0, false);
	glutStrokeString(GLUT_STROKE_ROMAN, (char*)"Esc- Exit", 3.5, -5.3, 5.0, 0.0017, 0.0017, 0.0017, 0.0, 1.0, 0.0, 0, false);
	glPopMatrix();
}